import sqlite3
import time
import socket

from Janela_main import Ui_MainWindow

from PySide2.QtWidgets import QApplication, QWidget, QTableWidget, QTableWidgetItem, QVBoxLayout
from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
                            QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
                           QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
                           QPixmap, QRadialGradient)
from PySide2 import QtWidgets


class janela(QtWidgets.QMainWindow, Ui_MainWindow):
    ip = '192.168.1.169'

    def __init__(self):
        super(janela, self).__init__()
        self.setupUi(self)

        banco = Comunic_Banco()

        ip = banco.Le_tabela_config()
        print(ip)

        self.tabela_Medicamentos.setColumnCount(12)
        self.tabela_Medicamentos.setHorizontalHeaderLabels(
            ["ID", "ID medicamento", "Fabricante", "Descrição", "Principio Ativo", "Comentário"])

        tabela = banco.Le_tabela_medicamento()
        i = 0
        j = 0
        for x in tabela:
            for y in x:
                if j == 0:
                    self.tabela_Medicamentos.setRowCount(i + 1)
                    self.tabela_Medicamentos.setItem(i, j, QTableWidgetItem(str(y)))
                else:
                    self.tabela_Medicamentos.setRowCount(i + 1)
                    self.tabela_Medicamentos.setItem(i, j, QTableWidgetItem(y))
                j = j + 1
            j = 0
            i = i + 1

        self.lineEdit.setText(" ")
        self.lineEdit_2.setText(" ")
        self.lineEdit_3.setText(" ")
        self.lineEdit_4.setText(" ")
        self.lineEdit_5.setText(" ")
        self.lineEdit_6.setText(" ")
        self.lineEdit_8.setText(" ")

        self.btn_pesquisa.clicked.connect(self.btn_pesquisa_metodo)
        self.btn_envio.clicked.connect(self.btn_envio_metodo)
        self.txt_folio.textChanged.connect(self.txt_folio_metodo)
        self.txt_lote.textChanged.connect(self.txt_lote_metodo)
        self.txt_vencimento.textChanged.connect(self.txt_vencimento_metodo)

    def txt_lote_metodo(self):
        self.lineEdit_4.setText("Vence: " + self.txt_vencimento.text() + " Lote: " + self.txt_lote.text())

    def txt_vencimento_metodo(self):
        self.lineEdit_4.setText("Vence: " + self.txt_vencimento.text() + " Lote: " + self.txt_lote.text())

    def txt_folio_metodo(self):
        self.lineEdit_3.setText("Folio: " + self.txt_folio.text())

    def btn_envio_metodo(self):
        comunicacao = envio()
        comunicacao.enviar_trabalho(self.ip, 21000, str.encode(self.txt_descricao.text()),
                                    str.encode(self.txt_principio_ativo.text()), str.encode(self.txt_folio.text()),
                                    str.encode(self.txt_vencimento.text()), str.encode(self.txt_lote.text()),
                                    str.encode(self.txt_fabricante.text()), str.encode(self.txt_comentario.text()),
                                    str.encode(self.txt_codigo.text()))

    def btn_pesquisa_metodo(self):
        banco = Comunic_Banco()
        tabela_mostrar = banco.pesquisa_item_banco(self.txt_pesquisa.text())

        self.txt_codigo.setText(tabela_mostrar[0][1])
        self.txt_descricao.setText(tabela_mostrar[0][3])
        self.txt_principio_ativo.setText(tabela_mostrar[0][4])
        self.txt_fabricante.setText(tabela_mostrar[0][2])
        self.txt_comentario.setText(tabela_mostrar[0][5])

        self.lineEdit_8.setText(tabela_mostrar[0][1])
        self.lineEdit.setText(tabela_mostrar[0][2])
        self.lineEdit_2.setText(tabela_mostrar[0][3])
        self.lineEdit_5.setText("Lab: " + tabela_mostrar[0][4])
        self.lineEdit_6.setText(tabela_mostrar[0][5])


class envio():
    def enviar_trabalho(self, ip, port, campo1, campo2, campo3, campo4, campo5, campo6, campo7, campo8):
        HOST2 = ip  # Endereco IP do Servidor
        PORT2 = port  # Porta que o Servidor esta
        tcp2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        dest2 = (HOST2, PORT2)

        print(ip)
        print('Iniciado')
        stx = b"\x02"
        etx = b"\x03"

        msg3 = stx + b'~JS0|Modelo Impressao|0|' + b'DS_PRODUTO|' + campo1 + b'|' + b'principio_ativo|' + campo2 + b'|' + b'Folio|' + campo3 + b'|' + b'VALIDADE|' + campo4 + b'|' + b'LOTE|' + campo5 + b'|' + b'FABRICANTE|' + campo6 + b'|' + b'comentario|' + campo7 + b'|' + b'CODE_128|' + campo8 + b'|' + etx

        print(msg3)

        try:
            print('Aguarda Conexão')
            tcp2.connect(dest2)
            tcp2.send(msg3)
        except:
            print("Falha Envio")


class Comunic_Banco(QtWidgets.QMainWindow, Ui_MainWindow):

    def Le_tabela_config(self):
        conn = sqlite3.connect('bd_integracao.db')
        print("banco conectado")
        cursor = conn.cursor()

        cursor.execute("""select * from config2""")
        teste = cursor.fetchall()

        return teste
        conn.close()

    def Le_tabela_medicamento(self):
        conn = sqlite3.connect('bd_integracao.db')
        print("banco conectado")
        cursor = conn.cursor()

        cursor.execute("""select * from medicamentos""")
        teste = cursor.fetchall()

        return teste
        conn.close()

    def Le_tabela_cortes(self, id_cortes):
        conn = sqlite3.connect('bd_integracao.db')
        print("banco conectado")
        cursor = conn.cursor()

        cursor.execute("""select * from cortes WHERE item = ?""", (id_cortes,))
        teste = cursor.fetchall()

        return teste

        # for linha in cursor.fetchall():
        #	print(linha)

        conn.close()

    def pesquisa_item_banco(self, item_pesquisar):
        print("Pesquins no Banco")

        conn = sqlite3.connect('bd_integracao.db')
        print("banco conectado")
        cursor = conn.cursor()

        cursor.execute("""select * from medicamentos WHERE id_medicamento = ?""", (item_pesquisar,))
        teste = cursor.fetchall()

        return teste

        conn.close()

    def cadastrar_item_banco(self, barras2, descricao2, codigo2, Fabricante):
        print("Pesquisa no Banco")

        conn = sqlite3.connect('bd_integracao.db')
        print("banco conectado")
        cursor = conn.cursor()

        cursor.execute("""
        INSERT INTO medicamentos (barras, decricao, codigo, fabricante)
        VALUES (?, ?, ?, ?)""", (barras2, descricao2, codigo2, Fabricante))

        conn.commit()
        conn.close()

    def cadastrar_cortes_banco(self, item):
        print("Pesquisa no Banco")

        conn = sqlite3.connect('bd_integracao.db')
        print("banco conectado")
        cursor = conn.cursor()

        for x in range(50):
            cursor.execute("""
            INSERT INTO cortes (item, posicao, X, Y, R, queda )
            VALUES (?, ?, ?, ?, ?, ?)""",
                           (item, (x + 1), 0, 0, 0, 0))

        conn.commit()
        conn.close()

    def Atualizar_cortes_banco(self, x, y, r, queda, item):
        print("Pesquisa no Banco")
        conn = sqlite3.connect('bd_integracao.db')
        print("banco conectado")
        cursor = conn.cursor()

        print(x)
        print(y)
        print(item)
        cursor.execute("""UPDATE cortes SET X = ?, Y = ?, R = ?, queda = ? WHERE id = ?""", (x, y, r, queda, item))

        conn.commit()
        conn.close()

    def deletar_item_banco(self, item, codigo3):
        print("Deletar item do Banco")
        conn = sqlite3.connect('bd_integracao.db')
        print("banco conectado")
        cursor = conn.cursor()
        cursor.execute("""DELETE FROM cortes WHERE item= ? """, (codigo3,))
        cursor.execute("""DELETE FROM medicamentos WHERE barras= ? """, (item,))
        conn.commit()
        conn.close()


if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    ui = janela()
    ui.show()
    sys.exit(app.exec_())
