import time
import socket
from Converter import *
import unicodedata

class conversor():
    def convert_unicode(valor):
        convertido = b''
        for c in valor:
            caractere = int('%04x' % ord(c), 16)
            convertido += caractere.to_bytes(2, 'little')
        
        return convertido

class envio():
    def enviar_trabalho(self, ip, port, campo1, campo2, campo3, campo4, campo5, campo6, campo7, campo8):
        HOST2 = ip  # Endereco IP do Servidor
        PORT2 = port  # Porta que o Servidor esta
        tcp2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        dest2 = (HOST2, PORT2)

        print(dest2)
        print(ip)
        print('Iniciado')
        stx = b"\x02"
        etx = b"\x03"
        msg3 = stx + b'~JS0|Modelo Impressao|0|' + b'DS_PRODUTO|' + campo1 + b'|' + b'principio_ativo|' + campo2 + b'|' + b'Folio|' + campo3 + b'|' + b'VALIDADE|' + campo4 + b'|' + b'LOTE|' + campo5 + b'|' + b'FABRICANTE|' + campo6 + b'|' + b'comentario|' + campo7 + b'|' + b'CODE_128|' + campo8 + b'|' + etx
        #msg4 = stringConvert(msg3)
        print(msg3)

        print('Aguarda Conexão')
        tcp2.connect(dest2)
        tcp2.send(msg3)
        tcp2.close()
        # print("Falha Envio")

    def enviar_trabalho_2(self, ip, port, nome_layout, campo1, campo2, campo3, campo4, campo5, campo6, campo7, campo8):
        HOST2 = ip  # Endereco IP do Servidor
        PORT2 = port  # Porta que o Servidor esta
        tcp2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        dest2 = (HOST2, PORT2)

        print(dest2)
        print(ip)
        print('Iniciado')
        stx = b"\x02"
        etx = b"\x03"
        msg3 = stx + b'~JS0|'+ nome_layout + b'|0|' + b'Generic_Name|'+ campo1 + b'|' + b'Commercial_Name|' + campo2 + b'|' + b'Lote|' + campo3 + b'|' + b'Med_ID|' + campo4 + b'|' + b'SKT|' + campo5 + b'|' + b'BARRAS|' + campo6 + b'|' + b'comentario|' + campo7 + b'|' + b'CODE_128|' + campo8 + b'|' + etx
        print(msg3)

        print('Aguarda Conexão')
        tcp2.connect(dest2)
        tcp2.send(msg3)
        tcp2.close()
        print("Enviado com sucesso")
  
    def enviar_trabalho_unicode(self, ip, port, nome_layout, campo1, campo2, campo3, campo4, campo5, campo6, campo7, campo8):
        HOST2 = ip  # Endereco IP do Servidor
        PORT2 = port  # Porta que o Servidor esta
        tcp2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        dest2 = (HOST2, PORT2)    

        print(dest2)
        print(ip)
        print('Iniciado')
        stx = b"\x02\x00"
        etx = b"\x03\x00"

        msg3 = stx 
        msg3 += conversor.convert_unicode("~JS0|")
        msg3 += conversor.convert_unicode(nome_layout) 
        msg3 += conversor.convert_unicode("|0|")
        msg3 += conversor.convert_unicode("Generic_Name|")
        msg3 += conversor.convert_unicode(campo1)
        msg3 += conversor.convert_unicode("|")
        msg3 += conversor.convert_unicode("Commercial_Name|")
        msg3 += conversor.convert_unicode(campo2) 
        msg3 += conversor.convert_unicode("|") 
        msg3 += conversor.convert_unicode("Lote|") 
        msg3 += conversor.convert_unicode(campo3) 
        msg3 += conversor.convert_unicode("|")
        msg3 += conversor.convert_unicode("Med_ID|")
        msg3 += conversor.convert_unicode(campo4) 
        msg3 += conversor.convert_unicode("|")
        msg3 += conversor.convert_unicode("SKT|") 
        msg3 += conversor.convert_unicode(campo5) 
        msg3 += conversor.convert_unicode("|")
        msg3 += conversor.convert_unicode("GS1EAN|") 
        msg3 += conversor.convert_unicode(campo6) 
        msg3 += conversor.convert_unicode("|")
        msg3 += conversor.convert_unicode("GS1VALIDADE|") 
        msg3 += conversor.convert_unicode(campo7) 
        msg3 += conversor.convert_unicode("|")
        msg3 += conversor.convert_unicode("CODE_128|") 
        msg3 += conversor.convert_unicode(campo8)
        msg3 += conversor.convert_unicode("|")
        msg3 += etx

        print(msg3)

        print('Aguarda Conexão')
        tcp2.connect(dest2)
        print('Conectado')
        tcp2.send(msg3)
        print('Enviado Pacote')
        #print(tcp2.recv(1024))
        tcp2.close()
        print("Enviado com sucesso")


