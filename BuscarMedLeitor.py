from BuscarMed_Leitor import *
from main import *

itemSelecionado = ''
colunaSelecionada = 0
linhaSelecionada = 0
infos = list()
linhaInfo = ''

class BuscarMedicamentoLeitor(QDialog):
    buffer = ""
    def __init__(self, label=None, idioma='Português - Brasil'):
        self.itemSelecionado = self.colunaSelecionada = 0
        super(BuscarMedicamentoLeitor, self).__init__()

        self.uiBusca = Ui_Dialog_leitor()
        self.uiBusca.setupUi(self)
        self.setWindowIcon(QIcon('imgs/logoJanela.png'))

        # Botões
        self.uiBusca.btn_Selecionar.clicked.connect(lambda: BuscarMedicamentoLeitor.Confirmar(self))
        self.uiBusca.btn_Cancelar.clicked.connect(lambda: BuscarMedicamentoLeitor.Sair(self))


    def keyPressEvent (self, event):
        print(event.key())
        if event.key() == 16777271:
            self.buffer += "#"
        elif event.key() == 16777251:
            self.buffer += "@"
        else:
            self.buffer += event.text()



    def Confirmar(self):
        if self.buffer[0] == "#":
            teste = self.buffer.split("#")

            gtin = teste[1]
            gtin = gtin[2:16]
            serial = teste[1]
            serial = serial[18:]
            Validade = teste[2]
            Validade = Validade[2:8]
            lote = teste[2]
            lote = lote[10:]

        else:
            teste = self.buffer.split("@")

            gtin = teste[0]
            gtin = gtin[2:16]
            serial = teste[0]
            serial = serial[18:]
            Validade = teste[1]
            Validade = Validade[3:9]
            lote = teste[1]
            lote = lote[11:]

        ano = Validade[0:2]
        mes = Validade[2:4]
        dia = Validade[4:6]

        Validade = dia + '/' + mes + '/' + ano
        validade_gs1 = ano + mes + dia
        global infos
        infos = [gtin,serial,Validade,lote,validade_gs1]

        self.buffer = ''
        self.close()

    def retornoInfo(self, valor):
        if 'medicamento' in valor:
            print(f'retorno Medicamento {valor}')
            return infos
        if 'usuario' in valor:
            print(f'retorno Usuario {valor}')
            return linhaInfo