"""
Entrada:
"~JS0|Modelo Impressao|0|DS_PRODUTO|campo1 |principio_ativo|campo2 |Folio|campo3|VALIDADE|campo4|LOTE|campo5 |FABRICANTE| campo6|comentario|campo7 |CODE_128|campo8 |")

Saida desejada:
SLA|Modelo Impressao|DS_PRODUTO=campo1 |principio_ativo=campo2 |Folio=campo3|VALIDADE=campo4|LOTE=campo5 |FABRICANTE=campo6|comentário=campo7 |CODE_128=campo8 |

OBS:. até a variável "0" é padrão e imutável.
        Após o "0" podem haver numeros maiores ou menores de campos.
"""


def stringConvert(valor):
    """
    A função recebe a string e trata ela da seguinte forma:
        Splita a string filtrando por | (pipe)
        Prepara o inicio da string com SLA | Nome | campo = campo1 | campo 2 = campo2........
        Faz um Strip para remover os espaços de cada slot do array
        faz a junção de todas as partes (concatenando)
        Adiciona 13 no final (como string)
        Retorna string inteira
    :param valor: String de entrada.
    :return: String formatada para ser utilizada na impressora MV videoJet
    """
    if 'ajuda' in valor:
        help(stringConvert)
    else:
        stringConvertida = ''
        stringSplit = valor.split('|')
        for i in range(0, len(stringSplit)):
            if i == 0:
                stringConvertida += 'SLA|'
            elif i == 1:
                stringConvertida += stringSplit[i]
                stringConvertida += '|'
            elif i == 2:
                pass
            else:
                if 'campo' in stringSplit[i]:
                    stringConvertida += '='
                    stringConvertida += stringSplit[i].strip()
                    stringConvertida += '|'
                else:
                    stringConvertida += stringSplit[i]
        final = '\x0D'
        stringConvertida += final
        return stringConvertida


# print(stringConvert(str(input('Valor de entrada: '))))  # Descomente para testar

# stringConvert(
# "~JS0|Modelo Impressao|0|DS_PRODUTO|campo1 |principio_ativo|campo2 |Folio|campo3|VALIDADE|campo4|LOTE|campo5 "
# "|FABRICANTE| campo6|comentario|campo7 |CODE_128|campo8 |")
