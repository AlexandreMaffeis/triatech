import os
import sqlite3
import sys
import platform
import pandas as pd
import unicodedata
from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QFrame, QGridLayout,
    QGroupBox, QHBoxLayout, QHeaderView, QLabel,
    QLayout, QLineEdit, QMainWindow, QPushButton,
    QSizePolicy, QSpacerItem, QStackedWidget, QTableWidget,
    QTableWidgetItem, QVBoxLayout, QWidget)
from datetime import datetime
# GUI FILE
import login
from ui_main import Ui_MainWindow
# IMPORT FUNCTIONS

from ui_functions import *
from db_functions import *
from NGCPL_functions import *
from BuscarMed import BuscarMedicamento
from BuscarMedLeitor import BuscarMedicamentoLeitor
from login import *

usuarioLogado = list()
cadastroAberto = False
excluirUsuario = ''


class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.lista = list()
        idioma = Comunic_Banco.setIdioma(self)
        self.checkIdioma(idioma[0][0])

        # Objeto instanciado para chamar a janela de busca
        self.buscarMed = BuscarMedicamento(None, idioma[0][0])

        self.ui.stackedWidget.setCurrentWidget(self.ui.page_1)

        ## TOGGLE/BURGUER MENU
        ########################################################################
        # self.ui.Btn_Toggle.clicked.connect(lambda: UIFunctions.toggleMenu(self, 250, True))

        ## PAGES
        ########################################################################

        # PAGE 1
        self.ui.btn_page_1.clicked.connect(self.btn_page_1_M)

        # PAGE 2
        self.ui.btn_page_2.clicked.connect(self.btn_page_2_M)

        # PAGE 3
        self.ui.btn_page_3.clicked.connect(self.btn_page_3_M)

        # PAGE 4
        self.ui.btn_page_4.clicked.connect(self.btn_page_4_M)

        # PAGE 5
        self.ui.btn_page_5.clicked.connect(self.btn_page_5_M)

        self.ui.btn_localiza_Prod.clicked.connect(lambda: MainWindow.btn_localiza_Prod_M(self))
        self.ui.btn_localiza_Usuario.clicked.connect(lambda: MainWindow.btn_localiza_Usuario_M(self))
        self.ui.btn_CadastrarUsuario.clicked.connect(lambda: MainWindow.CadastrarNovoUsuario(self))
        self.ui.btn_AtualizarCadastro.clicked.connect(lambda: MainWindow.AtualizarCadastro(self))
        self.ui.btn_ExcluirCadastro.clicked.connect(lambda: MainWindow.ExcluirCadastro(self))

        self.ui.btn_Atualiza_Config.clicked.connect(self.btn_Atualiza_Config_M)
        self.ui.btn_localiza_item.clicked.connect(self.btn_localiza_item_M)
        self.ui.btn_atualiza_item.clicked.connect(self.btn_atualiza_item_M)
        self.ui.btn_cadastra_item.clicked.connect(self.btn_cadastra_item_M)
        self.ui.btn_exclui_item.clicked.connect(self.btn_exclui_item_M)

        self.ui.btn_add_layout.clicked.connect(self.btn_add_layout_M)
        self.ui.btn_exp_excell.clicked.connect(self.btn_exp_excell_M)

        self.ui.btn_envio_prod.clicked.connect(self.btn_envio_prod_M)

        self.ui.lineEdit_folio.textChanged.connect(self.lineEdit_folio_M)
        self.ui.lineEdit_lote.textChanged.connect(self.lineEdit_lote_M)
        self.ui.lineEdit_vence.textChanged.connect(self.lineEdit_vence_M)

        layouts = Comunic_Banco.Le_tabela_layout(self)

        self.ui.comboBox_Nome_Layout.clear()

        for x in layouts:
            self.ui.comboBox_Nome_Layout.addItem(x[1])

        ## SHOW ==> MAIN WINDOW
        ########################################################################
        self.show()
        ## ==> END ##

    def btn_page_1_M(self):
        self.setCadastroOFF()
        self.ui.stackedWidget.setCurrentWidget(self.ui.page_1)

    def btn_page_2_M(self):
        self.setCadastroOFF()
        self.ui.stackedWidget.setCurrentWidget(self.ui.page_2)

        teste = Comunic_Banco.Le_tabela_usuario(self)

        self.ui.comboBox.clear()

        for x in teste:
            self.ui.comboBox.addItem(x[2])

        self.ui.label_26.setPixmap(QPixmap(u"imgs/datamatrix.JPG"))
        self.ui.label_5.setPixmap(QPixmap(u"imgs/Acıbadem_Grup_logo.png"))

    def btn_page_3_M(self):
        idioma = Comunic_Banco.setIdioma(self)
        idioma = idioma[0][0]
        self.setCadastroOFF()
        self.ui.stackedWidget.setCurrentWidget(self.ui.page_3)

        self.ui.tabela_historico.setColumnCount(12)
        if idioma == 'English':
            self.ui.tabela_historico.setHorizontalHeaderLabels(
                ["ID", "Medication ID", "Barcode", "Description", "Active principle", "Layout", "Serie", "Batch",
                 "Expiration", "Aprovation", "Date", "Hour"])
        elif idioma == 'Turkish':
            self.ui.tabela_historico.setHorizontalHeaderLabels(
                ["ID", "İlaç ID", "Barkod", "Etken Madde", "İlaç Adı", "Dizayn", "Seri No", "Lot No",
                 "SKT ", "Kullanıcı Onay", "Tarih", "Saat"])
        elif idioma == 'Español':
            self.ui.tabela_historico.setHorizontalHeaderLabels(
                ["ID", "ID Medicamento", "Fabricante", "Descripción", "Principio activo", "Alerta", "FOL", "Lote",
                 "Validez", "Aprobación", "Date", "Hour"])
        else:
            self.ui.tabela_historico.setHorizontalHeaderLabels(
                ["ID", "ID medicamento", "Fabricante", "Descrição", "Principio Ativo", "Comentário", "Folio", "Lote",
                 "Vence", "Aprovação", "Fecha", "Hora"])

        index = [0, 12, 6, 7, 8, 9, 10, 11, 13, 1, 2, 3, 4, 5, 14]

        tabela = Comunic_Banco.Le_tabela_producao(self)
        i = 0
        j = 0
        for x in tabela:
            for y in x:
                if j == 0:
                    self.ui.tabela_historico.setRowCount(i + 1)
                    self.ui.tabela_historico.setItem(i, index[j], QTableWidgetItem(str(y)))
                else:
                    self.ui.tabela_historico.setRowCount(i + 1)
                    self.ui.tabela_historico.setItem(i, index[j], QTableWidgetItem(y))
                j = j + 1
            j = 0
            i = i + 1

    def btn_exp_excell_M(self):
        columnHeaders = []

        # create column header list
        for j in range(self.ui.tabela_historico.model().columnCount()):
            columnHeaders.append(self.ui.tabela_historico.horizontalHeaderItem(j).text())

        df = pd.DataFrame(columns=columnHeaders)

        # create dataframe object recordset
        for row in range(self.ui.tabela_historico.rowCount()):
            for col in range(self.ui.tabela_historico.columnCount()):
                df.at[row, columnHeaders[col]] = self.ui.tabela_historico.item(row, col).text()

        df.to_excel('Histórico de Produção.xlsx', index=False)
        print('Excel dokümanı oluşturuldu')

    def btn_page_4_M(self):
        global usuarioLogado, cadastroAberto
        if cadastroAberto is False:
            idioma = Comunic_Banco.setIdioma(self)
            idioma = idioma[0][0]
            self.openLogin = LoginWindow(idioma)
            self.openLogin.setModal(True)
            self.openLogin.exec_()

            if self.openLogin.Confirmation():
                cadastroAberto = True
                usuarioLogado = self.openLogin.saveUser()
                print(usuarioLogado)
                self.ui.stackedWidget.setCurrentWidget(self.ui.page_4)
                print(f'Usuario logado: {usuarioLogado}')
                MainWindow.setNoValue(self, 'usuario')

    def btn_page_5_M(self):
        self.setCadastroOFF()
        self.ui.stackedWidget.setCurrentWidget(self.ui.page_5)
        Parametros = Comunic_Banco.Le_tabela_config(self)
        self.ui.lineEdit_IP.setText(Parametros[0][0])
        self.ui.lineEdit_Porta.setText(Parametros[0][1])

        layouts = Comunic_Banco.Le_tabela_layout(self)

        self.ui.cmb_cad_layout.clear()

        for x in layouts:
            self.ui.cmb_cad_layout.addItem(x[1])

    # Tela Config
    def btn_Atualiza_Config_M(self):
        Comunic_Banco.Atualizar_config_banco(self, self.ui.lineEdit_IP.text(), self.ui.lineEdit_Porta.text(),
                                             self.ui.cbb_Idioma.currentText(), '1')

        idioma = Comunic_Banco.setIdioma(self)
        self.checkIdioma(idioma[0][0])
        idioma = idioma[0][0]

        if idioma == 'English':
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('imgs/logoJanela.png'))
            msg.setWindowTitle("Information")
            msg.setIcon(QMessageBox.Warning)
            msg.setText(f'Changes will be fully loaded only\nafter restarting the software!')
            msg.exec_()

        if idioma == 'Turkish':
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('imgs/logoJanela.png'))
            msg.setWindowTitle("Bilgi")
            msg.setIcon(QMessageBox.Warning)
            msg.setText(f'Değişiklikler uygulama restart \nedildikten sonra yüklenecektir')
            msg.exec_()

        elif idioma == 'Español':
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('imgs/logoJanela.png'))
            msg.setWindowTitle("Información")
            msg.setIcon(QMessageBox.Warning)
            msg.setText(f'Los cambios se cargarán por completo solo \n nadespués de reiniciar el software.')
            msg.exec_()

        else:
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('imgs/logoJanela.png'))
            msg.setWindowTitle("Informação")
            msg.setIcon(QMessageBox.Warning)
            msg.setText(f'As alterações serão carregadas completamentes somente\napós reinicar o software!')
            msg.exec_()
    def btn_delete_layout_M(self):
        Comunic_Banco.deletar_layout_banco(self, self.ui.cmb_cad_layout.currentText())

        layouts = Comunic_Banco.Le_tabela_layout(self)
        self.ui.cmb_cad_layout.clear()

        for x in layouts:
            self.ui.cmb_cad_layout.addItem(x[1])

    def btn_add_layout_M(self):
        Comunic_Banco.cadastrar_layout_banco(self, self.ui.linedit_add_layout.text())

        layouts = Comunic_Banco.Le_tabela_layout(self)

        self.ui.cmb_cad_layout.clear()

        for x in layouts:
            self.ui.cmb_cad_layout.addItem(x[1])

    # tela Cadastra Item
    def btn_atualiza_item_M(self):
        Valor_lido = str.encode(self.ui.lineEdit_id_med_item.text())
        print(Valor_lido)
        #id_medicamento,barcode,descricao_medicamento,principio_ativo,layout)
        Comunic_Banco.Atualizar_item_banco(self, self.ui.lineEdit_id_med_item.text(), self.ui.lineEdit_alerta_item.text(),
                                           self.ui.lineEdit_Desc_item.text(), self.ui.lineEdit_princp_item.text(),
                                           self.ui.comboBox_Nome_Layout.currentText())

    def btn_cadastra_item_M(self):
        Comunic_Banco.cadastrar_item_banco(self, self.ui.lineEdit_id_med_item.text(), self.ui.lineEdit_alerta_item.text(),
                                           self.ui.lineEdit_Desc_item.text(), self.ui.lineEdit_princp_item.text(),
                                           self.ui.comboBox_Nome_Layout.currentText())

    def btn_exclui_item_M(self):
        Comunic_Banco.deletar_item_banco(self, self.ui.lineEdit_id_med_item.text())

        self.ui.lineEdit_id_med_item.setText(" ")
        self.ui.lineEdit_Desc_item.setText(" ")
        self.ui.lineEdit_princp_item.setText(" ")
        self.ui.lineEdit_alerta_item.setText(" ")

        layouts = Comunic_Banco.Le_tabela_layout(self)

        self.ui.comboBox_Nome_Layout.clear()

        for x in layouts:
            self.ui.comboBox_Nome_Layout.addItem(x[1])

    def btn_localiza_item_M(self):
        idioma = Comunic_Banco.setIdioma(self)
        self.checkIdioma(idioma[0][0])
        # Objeto instanciado para chamar a janela de busca
        self.buscarMed = BuscarMedicamento(None, idioma[0][0])
        self.buscarMed.setModal(True)
        self.buscarMed.exec_()

        idioma = Comunic_Banco.setIdioma(self)
        self.checkIdioma(idioma[0][0])

        valores = BuscarMedicamento.retornoInfo(self, 'medicamento')
        print("Valores Lidos pelo Leitor")
        print(valores)

        self.ui.lineEdit_id_med_item.setText(valores[1])
        self.ui.lineEdit_Desc_item.setText(valores[3])
        self.ui.lineEdit_princp_item.setText(valores[4])
        self.ui.lineEdit_vence_gs1.setText(valores[5])
        if 'None' not in valores[5]:
            self.ui.lineEdit_alerta_item.setText(valores[2])
        layouts = Comunic_Banco.Le_tabela_layout(self)

        self.ui.comboBox_Nome_Layout.clear()

        for x in layouts:
            self.ui.comboBox_Nome_Layout.addItem(x[1])

        self.ui.comboBox_Nome_Layout.setCurrentText(valores[5])

    # Tela Produção
    def btn_localiza_Prod_M(self):
        idioma = Comunic_Banco.setIdioma(self)
        self.checkIdioma(idioma[0][0])
        # Objeto instanciado para chamar a janela de busca
        self.buscarMedLeitor = BuscarMedicamentoLeitor(None, idioma[0][0])
        self.buscarMedLeitor.setModal(True)
        self.buscarMedLeitor.exec_()
        idioma = Comunic_Banco.setIdioma(self)
        self.checkIdioma(idioma[0][0])
        idioma = idioma[0][0]
        valores = BuscarMedicamentoLeitor.retornoInfo(self, 'medicamento')
        print("retorno da função busca")

        self.ui.lineEdit_folio.setText(valores[1])
        self.ui.lineEdit_vence.setText(valores[2])
        self.ui.lineEdit_lote.setText(valores[3])
        self.ui.lineEdit_vence_gs1.setText(valores[4])

        pesquisa = Comunic_Banco.pesquisa_item_banco(self, valores[0])
        #pesquisa = Comunic_Banco.pesquisa_item_banco(self, "08699591576029")

        print(pesquisa)

        self.ui.lineEdit_id_med_Prod.setText(pesquisa[0][1])
        self.ui.lineEdit_alerta_Prod.setText(pesquisa[0][2])
        self.ui.lineEdit_Desc_Prod.setText(pesquisa[0][3])
        self.ui.lineEdit_princp_Prod.setText(pesquisa[0][4])
        self.ui.nome_layout.setText(pesquisa[0][5])

        self.ui.lineEdit_3.setText(pesquisa[0][1])
        self.ui.lineEdit_alerta_Prod.setText(pesquisa[0][2])
        self.ui.lineEdit.setText(pesquisa[0][3])
        self.ui.lineEdit_2.setText(pesquisa[0][4])

        self.ui.lineEdit_3.setText(" Lot: " + self.ui.lineEdit_lote.text())
        self.ui.lineEdit_4.setText(" ILAÇ ID: " + self.ui.lineEdit_id_med_Prod.text())
        self.ui.lineEdit_5.setText("SKT: " + self.ui.lineEdit_vence.text())

        return valores

    def lineEdit_folio_M(self):
        print(" ")
        #self.ui.lineEdit_3.setText("Folio: " + self.ui.lineEdit_folio.text())

    def lineEdit_lote_M(self):
        self.ui.lineEdit_3.setText(" Lot: " + self.ui.lineEdit_lote.text())

    def lineEdit_vence_M(self):
        self.ui.lineEdit_5.setText("SKT: " + self.ui.lineEdit_vence.text())

    def btn_envio_prod_M(self):
        produto = Comunic_Banco.pesquisa_item_banco(self, self.ui.lineEdit_alerta_Prod.text())
        Parametros = Comunic_Banco.pesquisa_ABV_banco(self, str(self.ui.comboBox.currentText()))
        IpPorta = Comunic_Banco.Le_tabela_config(self)

        data_atual = datetime.now()
        data_em_texto = data_atual.strftime('%d/%m/%Y')
        hora_em_texto = data_atual.strftime('%H:/%M')

        if Parametros[0][4] == self.ui.lineEdit_senha_prod.text():
            print("Senha ok")
            Comunic_Banco.cadastrar_Producao_banco(self, produto[0][0], self.ui.lineEdit_folio.text(),
                                                   self.ui.lineEdit_lote.text(), self.ui.lineEdit_vence.text(),
                                                   self.ui.comboBox.currentText(), data_em_texto, hora_em_texto)

            # try:
            print("Enviar")
            #barras = str.encode(self.ui.lineEdit_id_med_Prod.text()) + str.encode('!') + str.encode(self.ui.lineEdit_vence.text()) + str.encode('!') + str.encode(self.ui.lineEdit_lote.text())
            #print(barras)
            #envio.enviar_trabalho_2(self, IpPorta[0][0], 21000,
            #                      str.encode(self.ui.nome_layout.text()),
            #                      str.encode(self.ui.lineEdit_Desc_Prod.text()),
            #                      str.encode(self.ui.lineEdit_princp_Prod.text()),
            #                      str.encode(self.ui.lineEdit_lote.text()),
            #                      str.encode(self.ui.lineEdit_id_med_Prod.text()),
            #                      str.encode(self.ui.lineEdit_vence.text()),
            #                      barras,
            #                      str.encode("Eliminado"),
            #                      str.encode(self.ui.lineEdit_alerta_Prod.text()))
            
            barras = self.ui.lineEdit_id_med_Prod.text() + '!' + self.ui.lineEdit_vence.text() + '!' + self.ui.lineEdit_lote.text()
            print(barras)
            envio.enviar_trabalho_unicode(self, IpPorta[0][0], 21000,
                                  self.ui.nome_layout.text(),
                                  self.ui.lineEdit_Desc_Prod.text(),
                                  self.ui.lineEdit_princp_Prod.text(),
                                  self.ui.lineEdit_lote.text(),
                                  self.ui.lineEdit_id_med_Prod.text(),
                                  self.ui.lineEdit_vence.text(),
                                  self.ui.lineEdit_alerta_Prod.text(),
                                  self.ui.lineEdit_vence_gs1.text(),
                                  self.ui.lineEdit_alerta_Prod.text())

            # except:
            #    print("Erro de Envio")
        else:
            print("senha Errada")

    # tela Usuário
    def btn_localiza_Usuario_M(self):
        global excluirUsuario
        idioma = Comunic_Banco.setIdioma(self)
        self.checkIdioma(idioma[0][0])
        self.bucarUser = BuscarMedicamento('Lista de Usuários', idioma[0][0])
        self.bucarUser.setModal(True)
        self.bucarUser.exec()

        try:
            excluirUsuario = valores = BuscarMedicamento.retornoInfo(self, 'usuario')
            print(valores)
            self.ui.cbb_Nivel.setCurrentText(valores[5])
            self.ui.lineEdit_Nome_usuario.setText(valores[1])
            self.ui.lineEdit_ABV_usuario.setText(valores[2])
            self.ui.lineEdit_cargo_usuario.setText(valores[3])
        except:
            pass

    def CadastrarNovoUsuario(self):
        nome = self.ui.lineEdit_Nome_usuario.text()
        abv = self.ui.lineEdit_ABV_usuario.text().lower()
        cargo = self.ui.lineEdit_cargo_usuario.text()
        nivel = self.ui.cbb_Nivel.currentText()
        senha = self.ui.lineEdit_senha_usuario.text()
        senhaConfirm = self.ui.lineEdit_SenhaConfirm.text()

        banco = sqlite3.connect('bd_integracao.db')
        BD = banco.cursor()
        BD.execute("""SELECT * FROM usuario WHERE abv = ?""", (abv,))
        info = BD.fetchall()

        if len(info):
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('imgs/logoJanela.png'))
            msg.setWindowTitle("Erro de Cadastro")
            msg.setIcon(QMessageBox.Warning)
            msg.setText(f'Essa breviatura já possui cadastro.\nFavor escolher uma ainda não Cadastrada!')
            msg.exec_()
        elif abv == '' or nome == '' or cargo == '' or senha == '' or senhaConfirm == '':
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('imgs/logoJanela.png'))
            msg.setWindowTitle("Erro de Cadastro")
            msg.setIcon(QMessageBox.Warning)
            msg.setText(f'Verifique se todos os campos foram preenchidos corretamente')
            msg.exec_()
        elif senha != senhaConfirm:
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('imgs/logoJanela.png'))
            msg.setWindowTitle("Erro de Cadastro")
            msg.setIcon(QMessageBox.Warning)
            msg.setText(f'Confirmação de senha diferente da senha inicial!')
            msg.exec_()
        else:
            try:
                BD.execute("""INSERT INTO usuario(nome, abv, carg, senha, nivel) VALUES(?,?,?,?,?)""",
                           (nome, abv, cargo, senha, nivel))
                banco.commit()
                banco.close()
                print('Cadastro Concluido')
                msg = QMessageBox()
                msg.setWindowIcon(QIcon('imgs/logoJanela.png'))
                msg.setWindowTitle("Confirmação de Cadastro")
                msg.setIcon(QMessageBox.Information)
                msg.setText(f'{nome} cadastrado com sucesso!')
                msg.exec_()
            except:
                print('Erro ao cadastrar')

    def AtualizarCadastro(self):
        nome = self.ui.lineEdit_Nome_usuario.text()
        abv = self.ui.lineEdit_ABV_usuario.text().lower()
        cargo = self.ui.lineEdit_cargo_usuario.text()
        nivel = self.ui.cbb_Nivel.currentText()
        senha = self.ui.lineEdit_senha_usuario.text()
        senhaConfirm = self.ui.lineEdit_SenhaConfirm.text()
        senhaAtual = self.ui.txt_SenhaAntiga.text()

        banco = sqlite3.connect('bd_integracao.db')
        BD = banco.cursor()
        BD.execute("""SELECT * FROM usuario WHERE abv = ?""", (abv,))
        info = BD.fetchall()
        if len(abv) > 0:
            if info:
                if senha != '' and senha == senhaConfirm:
                    if senhaAtual == info[0][4]:
                        try:
                            BD.execute("""UPDATE usuario SET nivel = ?, senha = ? WHERE abv = ?""", (nivel, senha, abv))
                            banco.commit()
                            print('Alterações salvas com sucesso!')
                        except:
                            print('Erro ao atualizar dados (Att Senha)')
                elif senha == '' and senhaConfirm == '':
                    try:
                        BD.execute("""UPDATE usuario SET nivel = ? WHERE abv = ?""", (nivel, abv))
                        print('Alterações salvas com sucesso!')
                        banco.commit()
                    except:
                        print('Erro ao atualizar dados (Nivel)')
                else:
                    msg = QMessageBox()
                    msg.setWindowIcon(QIcon('imgs/logoJanela.png'))
                    msg.setWindowTitle("Erro na atualização!")
                    msg.setIcon(QMessageBox.Information)
                    msg.setText(f'Senha Incorreta!')
                    msg.exec_()
            else:
                msg = QMessageBox()
                msg.setWindowIcon(QIcon('imgs/logoJanela.png'))
                msg.setWindowTitle("Erro na atualização!")
                msg.setIcon(QMessageBox.Critical)
                msg.setText(f'Nenhum usuário correspondem a abreviação: {abv}')
                msg.exec_()
        else:
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('imgs/logoJanela.png'))
            msg.setWindowTitle("Erro na atualização!")
            msg.setIcon(QMessageBox.Warning)
            msg.setText(f'Verifique o campo Abreviação!')
            msg.exec_()
        banco.close()

    def ExcluirCadastro(self):
        global excluirUsuario
        print(excluirUsuario)
        try:
            banco = sqlite3.connect('bd_integracao.db')
            DB = banco.cursor()
            DB.execute("""DELETE FROM usuario WHERE abv = ?""", (excluirUsuario[2],))
            banco.commit()
            banco.close()

            msg = QMessageBox()
            msg.setWindowIcon(QIcon('imgs/logoJanela.png'))
            msg.setWindowTitle("Informação")
            msg.setIcon(QMessageBox.Information)
            msg.setText(f'Usuário excluido com sucesso!')
            msg.exec_()
        except:
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('imgs/logoJanela.png'))
            msg.setWindowTitle("Erro!")
            msg.setIcon(QMessageBox.Warning)
            msg.setText(f'Verifique as informações de exclusão')
            msg.exec_()

    def setCadastroOFF(self):
        global cadastroAberto
        cadastroAberto = False

    def setNoValue(self, page):
        if 'usuario' in page:
            self.ui.lineEdit_Nome_usuario.setText('')
            self.ui.lineEdit_ABV_usuario.setText('')
            self.ui.lineEdit_senha_usuario.setText('')
            self.ui.lineEdit_SenhaConfirm.setText('')
            self.ui.lineEdit_cargo_usuario.setText('')

    def checkIdioma(self, idioma):
        if idioma == 'English':
            # Tradução side menu
            self.ui.btn_page_1.setText('Item')
            self.ui.btn_page_2.setText('Production')
            self.ui.btn_page_3.setText('Historic')
            self.ui.btn_page_4.setText('Users')
            self.ui.btn_page_5.setText('Configurations')

            # Tradução Cadastro de Itens
            self.ui.label_7.setText('Item Registration')
            self.ui.groupBox.setTitle('Item Registration')
            self.ui.label_6.setText('Medication ID')
            self.ui.label_9.setText('Description')
            self.ui.label_4.setText('Active Principle')
            self.ui.label_10.setText('Barcode')
            self.ui.btn_localiza_item.setText('Search')
            self.ui.btn_cadastra_item.setText('Register')
            self.ui.btn_atualiza_item.setText('Update')
            self.ui.btn_exclui_item.setText('Delete')

            # Tradução de configurações
            self.ui.label_29.setText('Configurations')
            self.ui.groupBox_9.setTitle('Configurations')
            self.ui.label_24.setText('Port')
            self.ui.label_32.setText('Language')
            self.ui.btn_Atualiza_Config.setText('Update')

            # Tradução de Cadastro de usuarios
            self.ui.label_16.setText('User Registration')
            self.ui.groupBox_3.setTitle('User')
            self.ui.label_31.setText('Level')
            self.ui.label_13.setText('Name')
            self.ui.label_11.setText('User Name')
            self.ui.label_12.setText('Password')
            self.ui.label_15.setText('Password')
            self.ui.label_14.setText('Ocupation')
            self.ui.label.setText('Current Password')
            self.ui.btn_localiza_Usuario.setText('Search Registration')
            self.ui.btn_CadastrarUsuario.setText('Register')
            self.ui.btn_ExcluirCadastro.setText('Delete')
            self.ui.btn_AtualizarCadastro.setText('Update Register')

            # Traduçãoo de Historico de produção
            self.ui.label_28.setText('Production Historic')
            self.ui.groupBox_7.setTitle('Historic')

            # Tradução Produção
            self.ui.label_22.setText('Production')
            self.ui.groupBox_5.setTitle('Item Search')
            self.ui.label_19.setText('Medication ID')
            self.ui.label_20.setText('Description')
            self.ui.label_17.setText('Active principle')
            self.ui.label_18.setText('Barcode')
            self.ui.groupBox_6.setTitle('Production data')
            self.ui.label_27.setText('Serie')
            self.ui.label_30.setText('Batch')
            self.ui.label_25.setText('Expiration')
            self.ui.label_21.setText('Expiration  (GS1)')
            self.ui.label_2.setText('Aprovation')
            self.ui.btn_envio_prod.setText('Send')
            self.ui.btn_localiza_Prod.setText('Find Item')

        if idioma == 'Turkish':
            # Tradução side menu
            self.ui.btn_page_1.setText('Kalem')
            self.ui.btn_page_2.setText('Formüler')
            self.ui.btn_page_3.setText('Kayıtlar')
            self.ui.btn_page_4.setText('Kullanıcılar')
            self.ui.btn_page_5.setText('Konfigürasyonlar')

            # Tradução Cadastro de Itens
            self.ui.label_7.setText('İlaç Kaydı')
            self.ui.groupBox.setTitle('İlaç Kaydı')
            self.ui.label_6.setText('İlaç ID')
            self.ui.label_9.setText('Etken Madde')
            self.ui.label_4.setText('İlaç Adı')
            self.ui.label_10.setText('Barkod')
            self.ui.btn_localiza_item.setText('Ara')
            self.ui.btn_cadastra_item.setText('Kayıt')
            self.ui.btn_atualiza_item.setText('Güncelle')
            self.ui.btn_exclui_item.setText('Sil')

            # Tradução de configurações
            self.ui.label_29.setText('Konfigürasyonlar')
            self.ui.groupBox_9.setTitle('Konfigürasyonlar')
            self.ui.label_24.setText('Port')
            self.ui.label_32.setText('Dil')
            self.ui.btn_Atualiza_Config.setText('Güncelle')

            # Tradução de Cadastro de usuarios
            self.ui.label_16.setText('Kullanıcı Kayıt')
            self.ui.groupBox_3.setTitle('Kullanıcı')
            self.ui.label_31.setText('Rol')
            self.ui.label_13.setText('Ad')
            self.ui.label_11.setText('Soyad')
            self.ui.label_12.setText('Şifre')
            self.ui.label_15.setText('Şifre')
            self.ui.label_14.setText('Görev')
            self.ui.label.setText('Güncel Şifre')
            self.ui.btn_localiza_Usuario.setText('Kullanıcıları Ara')
            self.ui.btn_CadastrarUsuario.setText('Kayıt')
            self.ui.btn_ExcluirCadastro.setText('Sil')
            self.ui.btn_AtualizarCadastro.setText('Bilgileri Güncelle')

            # Traduçãoo de Historico de produção
            self.ui.label_28.setText('İşlem Kayıtları')
            self.ui.groupBox_7.setTitle('Kayıtlar')

            # Tradução Produção
            self.ui.label_22.setText('Formüler')
            self.ui.groupBox_5.setTitle('İlaç Arama')
            self.ui.label_19.setText('İlaç ID')
            self.ui.label_20.setText('İlaç Adı')
            self.ui.label_17.setText('Etken Madde')
            self.ui.label_18.setText('Barkod')
            self.ui.groupBox_6.setTitle('Ürün Datası')
            self.ui.label_27.setText('Seri No')
            self.ui.label_30.setText('Lot No')
            self.ui.label_25.setText('SKT ')
            self.ui.label_21.setText('SKT (GS1)')
            self.ui.label_2.setText('Kullanıcı Onay')
            self.ui.btn_envio_prod.setText('Gönder')
            self.ui.btn_localiza_Prod.setText('İlaç Arama')

        if idioma == 'Español':
            # Tradução side menu
            self.ui.btn_page_1.setText('Artículo')
            self.ui.btn_page_2.setText('Producción')
            self.ui.btn_page_3.setText('Histórico')
            self.ui.btn_page_4.setText('Usuarios')
            self.ui.btn_page_5.setText('Ajustes')

            # Tradução Cadastro de Itens
            self.ui.label_7.setText('Registrar artículos')
            self.ui.groupBox.setTitle('Registro de artículos')
            self.ui.label_6.setText('ID Medicamento')
            self.ui.label_9.setText('Descripción')
            self.ui.label_4.setText('Principio activo')
            self.ui.label_10.setText('Codigo de Barras')
            self.ui.btn_localiza_item.setText('Buscar')
            self.ui.btn_cadastra_item.setText('Registrar')
            self.ui.btn_atualiza_item.setText('Actualizar')
            self.ui.btn_exclui_item.setText('Borrar')

            # Tradução de configurações
            self.ui.label_29.setText('Ajustes')
            self.ui.groupBox_9.setTitle('Ajustes')
            self.ui.label_24.setText('Puerto')
            self.ui.label_32.setText('Idioma')
            self.ui.btn_Atualiza_Config.setText('Actualizar')

            # Tradução de Cadastro de usuarios
            self.ui.label_16.setText('Registro de usuario')
            self.ui.groupBox_3.setTitle('Usuario')
            self.ui.label_31.setText('Nivel')
            self.ui.label_13.setText('Nombre')
            self.ui.label_11.setText('Nombre de Usuario')
            self.ui.label_12.setText('Contraseña')
            self.ui.label_15.setText('Contraseña')
            self.ui.label_14.setText('Oficina')
            self.ui.label.setText('Contraseña actual')
            self.ui.btn_localiza_Usuario.setText('Encontrar registro')
            self.ui.btn_CadastrarUsuario.setText('Registro')
            self.ui.btn_ExcluirCadastro.setText('Borrar')
            self.ui.btn_AtualizarCadastro.setText('Actualizar Registro')

            # Traduçãoo de Historico de produção
            self.ui.label_28.setText('Historial de producción')
            self.ui.groupBox_7.setTitle('Histórico')

            # Tradução Produção
            self.ui.label_22.setText('Producción')
            self.ui.groupBox_5.setTitle('Buscar medicina')
            self.ui.label_19.setText('ID Medicamento')
            self.ui.label_20.setText('Descripción')
            self.ui.label_17.setText('Principio activo')
            self.ui.label_18.setText('Código de Barras')
            self.ui.groupBox_6.setTitle('Datos de produccion')
            self.ui.label_27.setText('FOL')
            self.ui.label_30.setText('Lote')
            self.ui.label_25.setText('Validez')
            self.ui.label_21.setText('Validez (GS1)')
            self.ui.label_2.setText('Aprobación')
            self.ui.btn_envio_prod.setText('Mandar')
            self.ui.btn_localiza_Prod.setText('Buscar')


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())
