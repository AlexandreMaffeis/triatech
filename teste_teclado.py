from tkinter import *


class KeyboardEvent(Frame):
    def __init__(self):
        Frame.__init__(self)
        self.pack(expand=YES, fill=BOTH)
        self.master.title("Eventos de teclado")
        self.master.geometry("250x50")

        # Label e Stringvar que vao exibir a tecla
        self.mensagem = StringVar()
        self.label = Label(self, textvariable=self.mensagem)
        self.mensagem.set("Aperte uma tecla")
        self.label.pack()

        # Fazendo os binding no frame
        self.master.bind("<KeyPress>", self.teclaPressionada)
        self.master.bind("<KeyRelease>", self.teclaLiberada)
        self.master.bind("<KeyPress-Alt_L>", self.altPressionado)
        self.master.bind("<KeyRelease-Alt_L>", self.altLiberado)

        mainloop()

    def teclaPressionada(self, event):
        print("tecla pressionada = " + event.char + " KeyCode = " + str(event.keycode))

        self.mensagem.set("Tecla pressionada: " + event.char)

    def teclaLiberada(self, event):
        self.mensagem.set("Tecla solta: " + event.char)

    def altPressionado(self, event):
        self.mensagem.set("Alt pressionado")

    def altLiberado(self, event):
        self.mensagem.set("Alt liberado")


# Chamando a classe
KeyboardEvent()