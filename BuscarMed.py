from ui_BuscarMed import *
from main import *

from PySide6.QtGui import QIcon, QStandardItemModel, QStandardItem
from PySide6.QtCore import QSortFilterProxyModel

from PySide6 import QtGui, QtCore, QtWidgets

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QDialog, QFrame, QHBoxLayout,
    QHeaderView, QLabel, QLineEdit, QPushButton,
    QSizePolicy, QTableView, QVBoxLayout, QWidget, QAbstractItemView)

itemSelecionado = ''
colunaSelecionada = 0
linhaSelecionada = 0
infos = list()
linhaInfo = ''


class BuscarMedicamento(QDialog):
    def __init__(self, label=None, idioma='Português - Brasil'):
        self.itemSelecionado = self.colunaSelecionada = 0
        super(BuscarMedicamento, self).__init__()

        self.uiBusca = Ui_Dialog()
        self.uiBusca.setupUi(self)
        self.setWindowIcon(QIcon('imgs/logoJanela.png'))

        # Botões
        self.uiBusca.btn_Selecionar.clicked.connect(lambda: BuscarMedicamento.Confirmar(self))
        self.uiBusca.btn_Cancelar.clicked.connect(lambda: BuscarMedicamento.Sair(self))

        if label is None:
            # Objeto instanciado para manipular textos da janela principal.
            banco = sqlite3.connect('bd_integracao.db')
            BD = banco.cursor()
            BD.execute("""SELECT * FROM medicamentos""")
            informacoes = BD.fetchall()
            banco.close()

            model = QStandardItemModel(len(informacoes), 6)

            # Idioma Ingles
            if idioma == 'English':
                self.uiBusca.label.setText('Medications List')
                self.uiBusca.txt_Pesquisar.setPlaceholderText('Search')
                self.uiBusca.btn_Selecionar.setText('Select')
                self.uiBusca.btn_Cancelar.setText('Cancel')
                model.setHorizontalHeaderLabels(
                    ['ID', 'Medication ID', 'Manufacturer', 'Description', 'Active Principle', 'Alert'])

            # Idioma Espanhol
            elif idioma == 'Español':
                self.uiBusca.label.setText('Lista de medicamentos')
                self.uiBusca.txt_Pesquisar.setPlaceholderText('Buscar')
                self.uiBusca.btn_Selecionar.setText('Seleccione')
                self.uiBusca.btn_Cancelar.setText('Cancelar')
                model.setHorizontalHeaderLabels(
                    ['ID', 'ID Medicamento', 'Fabricante', 'Descripción', 'Principio activo', 'Alerta'])

            # Idioma Portugues
            else:
                self.uiBusca.label.setText('Lista de Medicamentos')
                model.setHorizontalHeaderLabels(
                    ['ID', 'ID Medicamento', 'Fabricante', 'Descrição', 'Princípio ativo', 'Comentário'])

            for r, info in enumerate(informacoes):
                for colum, item in enumerate(info):
                    if type(item):
                        item = str(item)
                    elemento = QStandardItem(item)  # OBS: Somente Strings!!!!!

                    # Armazena dentro do objeto informação (LINHA da info, Coluna da info, info)
                    model.setItem(r, colum, elemento)

            filtroBusca = QSortFilterProxyModel()
            filtroBusca.setSourceModel(model)
            filtroBusca.setFilterKeyColumn(3)
            filtroBusca.setFilterCaseSensitivity(Qt.CaseInsensitive)

            self.uiBusca.tb_Lista.setModel(filtroBusca)
            self.uiBusca.tb_Lista.verticalHeader().setVisible(False)
            self.uiBusca.tb_Lista.horizontalHeader().setSectionResizeMode(QHeaderView.Interactive)
            self.uiBusca.txt_Pesquisar.textChanged.connect(filtroBusca.setFilterRegularExpression)

            # Seta a ultima coluna para ocupar to do espaco presente
            self.uiBusca.tb_Lista.horizontalHeader().setStretchLastSection(True)
            # Bloqueia as celulas para edicao
            self.uiBusca.tb_Lista.setEditTriggers(QAbstractItemView.NoEditTriggers)

            # Reconhece o Click Na tabela.
            self.uiBusca.tb_Lista.clicked.connect(lambda: BuscarMedicamento.selecionado(self))
            # Tamanho da coluna ID
            self.uiBusca.tb_Lista.setColumnWidth(0, 40)
            # Tamanho da coluna ID medicamento
            self.uiBusca.tb_Lista.setColumnWidth(1, 100)
            # Tamanho da coluna Fabricante
            self.uiBusca.tb_Lista.setColumnWidth(2, 120)
            # Tamanho da coluna Descricao
            self.uiBusca.tb_Lista.setColumnWidth(3, 150)
            # Tamanho da coluna Principio
            self.uiBusca.tb_Lista.setColumnWidth(4, 400)

        if label is not None:
            if label == 'Lista de Usuários':
                global linhaInfo
                linhaInfo = ''
                banco = sqlite3.connect('bd_integracao.db')
                BD = banco.cursor()
                BD.execute("""SELECT * FROM usuario""")
                usuarios = BD.fetchall()
                modelUser = QStandardItemModel(len(usuarios), 5)

                if idioma == 'English':
                    self.uiBusca.txt_Pesquisar.setPlaceholderText('Search')
                    self.uiBusca.label.setText('Users List')
                    self.uiBusca.btn_Selecionar.setText('Select')
                    self.uiBusca.btn_Cancelar.setText('Cancel')
                    modelUser.setHorizontalHeaderLabels(['ID', 'Name', 'User Name', 'Ocupation', 'Password', 'Level'])
                elif idioma == 'Español':
                    self.uiBusca.txt_Pesquisar.setPlaceholderText('Buscar')
                    self.uiBusca.label.setText('Lista de usuarios')
                    self.uiBusca.btn_Selecionar.setText('Seleccione')
                    self.uiBusca.btn_Cancelar.setText('Cancelar')
                    modelUser.setHorizontalHeaderLabels(
                        ['ID', 'Nombre', 'Nombre de Usuario', 'ocupación', 'Contraseña', 'Nivel'])
                else:
                    self.uiBusca.label.setText('Lista de Usuários')
                    modelUser.setHorizontalHeaderLabels(['ID', 'Nome', 'Abreviação', 'Cargo', 'Senha', 'Nivel'])

                self.setMinimumSize(QSize(600, 400))
                self.setMaximumSize(QSize(600, 400))

                for r, info in enumerate(usuarios):
                    for colum, item in enumerate(info):
                        if isinstance(item, int):
                            item = str(item)
                        elementoUser = QStandardItem(item)
                        modelUser.setItem(r, colum, elementoUser)

                filtroBuscaUser = QSortFilterProxyModel()
                filtroBuscaUser.setSourceModel(modelUser)
                filtroBuscaUser.setFilterKeyColumn(1)
                filtroBuscaUser.setFilterCaseSensitivity(Qt.CaseInsensitive)

                self.uiBusca.tb_Lista.setModel(filtroBuscaUser)
                self.uiBusca.tb_Lista.verticalHeader().setVisible(False)
                self.uiBusca.tb_Lista.horizontalHeader().setSectionResizeMode(QHeaderView.Interactive)
                self.uiBusca.txt_Pesquisar.textChanged.connect(filtroBuscaUser.setFilterRegExp)

                # Seta a ultima coluna para ocupar to do espaco presente
                self.uiBusca.tb_Lista.horizontalHeader().setStretchLastSection(True)
                # Bloqueia as celulas para edicao
                self.uiBusca.tb_Lista.setEditTriggers(QAbstractItemView.NoEditTriggers)
                # Oculta coluna de senha
                self.uiBusca.tb_Lista.hideColumn(4)

                # Reconhece o Click Na tabela.
                self.uiBusca.tb_Lista.clicked.connect(lambda: BuscarMedicamento.selecionado(self))
                # Tamanho da coluna ID
                self.uiBusca.tb_Lista.setColumnWidth(0, 40)
                # Tamanho da coluna Nome
                self.uiBusca.tb_Lista.setColumnWidth(1, 200)
                # Tamanho da coluna Abreviação
                self.uiBusca.tb_Lista.setColumnWidth(2, 100)
                # Tamanho da coluna Cargo
                self.uiBusca.tb_Lista.setColumnWidth(3, 130)
                # Tamanho da coluna Senha
                self.uiBusca.tb_Lista.setColumnWidth(4, 80)
                # Tamanho da coluna Nivel
                self.uiBusca.tb_Lista.setColumnWidth(5, 40)

    def selecionado(self):
        global itemSelecionado, colunaSelecionada, linhaSelecionada, linhaInfo

        for _index in self.uiBusca.tb_Lista.selectionModel().selectedIndexes():
            colunaSelecionada = _index.column()
            linhaSelecionada = _index.row()
            itemSelecionado = _index.data()

            linhaInfo = list()
            for column in range(self.uiBusca.tb_Lista.model().columnCount()):  # Range = numero de colunas
                index = self.uiBusca.tb_Lista.model().index(linhaSelecionada, column)
                linhaInfo.append(index.data())
            print(linhaInfo)

    def Sair(self):
        self.close()

    def Confirmar(self):
        global itemSelecionado, colunaSelecionada
        # print(f'{itemSelecionado[0]}')
        coluna = self.confirmColumn(colunaSelecionada)
        # print(f'Conluna = {coluna} do tipo = {type(coluna)}')

        global infos
        infos = linhaInfo
        self.close()

    def confirmColumn(self, columnNumber):
        if columnNumber == 0:
            return 'id'
        elif columnNumber == 1:
            return 'id_medicamento'
        elif columnNumber == 2:
            return 'fabricante'
        elif columnNumber == 3:
            return 'descricao_medicamento'
        elif columnNumber == 4:
            return 'principio_ativo'
        else:
            return 'comentario'

    def retornoInfo(self, valor):
        if 'medicamento' in valor:
            print(f'retorno Medicamento {valor}')
            return infos
        if 'usuario' in valor:
            print(f'retorno Usuario {valor}')
            return linhaInfo
