# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_main.ui'
##
## Created by: Qt User Interface Compiler version 6.4.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QFrame, QGridLayout,
    QGroupBox, QHBoxLayout, QHeaderView, QLabel,
    QLayout, QLineEdit, QMainWindow, QPushButton,
    QSizePolicy, QSpacerItem, QStackedWidget, QTableWidget,
    QTableWidgetItem, QVBoxLayout, QWidget)
import resources_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1000, 557)
        MainWindow.setMinimumSize(QSize(1000, 500))
        icon = QIcon()
        icon.addFile(u":/imgs/imgs/logoJanela.png", QSize(), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setStyleSheet(u"background-color: rgb(214, 214, 214);")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.Top_Bar = QFrame(self.centralwidget)
        self.Top_Bar.setObjectName(u"Top_Bar")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.Top_Bar.sizePolicy().hasHeightForWidth())
        self.Top_Bar.setSizePolicy(sizePolicy)
        self.Top_Bar.setMinimumSize(QSize(0, 80))
        self.Top_Bar.setMaximumSize(QSize(16777215, 80))
        self.Top_Bar.setStyleSheet(u"background-color: rgb(0, 0, 182);")
        self.Top_Bar.setFrameShape(QFrame.NoFrame)
        self.Top_Bar.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.Top_Bar)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.frame_toggle = QFrame(self.Top_Bar)
        self.frame_toggle.setObjectName(u"frame_toggle")
        self.frame_toggle.setMinimumSize(QSize(0, 80))
        self.frame_toggle.setMaximumSize(QSize(120, 80))
        self.frame_toggle.setStyleSheet(u"background-color: rgb(0, 0, 182);")
        self.frame_toggle.setFrameShape(QFrame.NoFrame)
        self.frame_toggle.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.frame_toggle)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.label_8 = QLabel(self.frame_toggle)
        self.label_8.setObjectName(u"label_8")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.label_8.sizePolicy().hasHeightForWidth())
        self.label_8.setSizePolicy(sizePolicy1)
        self.label_8.setMinimumSize(QSize(120, 0))
        self.label_8.setStyleSheet(u"background-color: white;")
        self.label_8.setPixmap(QPixmap(u"imgs/logo.jpg"))
        self.label_8.setScaledContents(True)

        self.verticalLayout_2.addWidget(self.label_8)


        self.horizontalLayout.addWidget(self.frame_toggle)

        self.frame_top = QFrame(self.Top_Bar)
        self.frame_top.setObjectName(u"frame_top")
        self.frame_top.setMaximumSize(QSize(16777215, 1111111))
        self.frame_top.setFrameShape(QFrame.StyledPanel)
        self.frame_top.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_3 = QHBoxLayout(self.frame_top)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.label_3 = QLabel(self.frame_top)
        self.label_3.setObjectName(u"label_3")
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        font = QFont()
        font.setPointSize(36)
        font.setBold(False)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet(u"color: rgb(255, 255, 255);")

        self.horizontalLayout_3.addWidget(self.label_3, 0, Qt.AlignHCenter)


        self.horizontalLayout.addWidget(self.frame_top)


        self.verticalLayout.addWidget(self.Top_Bar)

        self.Content = QFrame(self.centralwidget)
        self.Content.setObjectName(u"Content")
        self.Content.setFrameShape(QFrame.NoFrame)
        self.Content.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.Content)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.frame_left_menu = QFrame(self.Content)
        self.frame_left_menu.setObjectName(u"frame_left_menu")
        self.frame_left_menu.setMinimumSize(QSize(150, 0))
        self.frame_left_menu.setMaximumSize(QSize(70, 16777215))
        self.frame_left_menu.setStyleSheet(u"background-color: rgb(255, 255, 127);")
        self.frame_left_menu.setFrameShape(QFrame.StyledPanel)
        self.frame_left_menu.setFrameShadow(QFrame.Raised)
        self.verticalLayout_3 = QVBoxLayout(self.frame_left_menu)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.frame_top_menus = QFrame(self.frame_left_menu)
        self.frame_top_menus.setObjectName(u"frame_top_menus")
        self.frame_top_menus.setFrameShape(QFrame.NoFrame)
        self.frame_top_menus.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.frame_top_menus)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.btn_page_1 = QPushButton(self.frame_top_menus)
        self.btn_page_1.setObjectName(u"btn_page_1")
        self.btn_page_1.setMinimumSize(QSize(0, 40))
        font1 = QFont()
        font1.setPointSize(12)
        font1.setBold(True)
        self.btn_page_1.setFont(font1)
        self.btn_page_1.setStyleSheet(u"QPushButton {\n"
"	color: #616161;\n"
"	background-color: rgb(255, 255, 127);\n"
"	border: 0px solid;\n"
"}\n"
"QPushButton:hover {\n"
"	background-color: rgb(255, 255, 0);\n"
"	color: black;\n"
"	border-top: 1px solid black;\n"
"	border-bottom: 1px solid black;\n"
"}")

        self.verticalLayout_4.addWidget(self.btn_page_1)

        self.btn_page_2 = QPushButton(self.frame_top_menus)
        self.btn_page_2.setObjectName(u"btn_page_2")
        self.btn_page_2.setMinimumSize(QSize(0, 40))
        self.btn_page_2.setFont(font1)
        self.btn_page_2.setStyleSheet(u"QPushButton {\n"
"	color: #616161;\n"
"	background-color: rgb(255, 255, 127);\n"
"	border: 0px solid;\n"
"}\n"
"QPushButton:hover {\n"
"	background-color: rgb(255, 255, 0);\n"
"	color: black;\n"
"	border-top: 1px solid black;\n"
"	border-bottom: 1px solid black;\n"
"}")

        self.verticalLayout_4.addWidget(self.btn_page_2)

        self.btn_page_3 = QPushButton(self.frame_top_menus)
        self.btn_page_3.setObjectName(u"btn_page_3")
        self.btn_page_3.setMinimumSize(QSize(0, 40))
        self.btn_page_3.setFont(font1)
        self.btn_page_3.setStyleSheet(u"QPushButton {\n"
"	color: #616161;\n"
"	background-color: rgb(255, 255, 127);\n"
"	border: 0px solid;\n"
"}\n"
"QPushButton:hover {\n"
"	background-color: rgb(255, 255, 0);\n"
"	color: black;\n"
"	border-top: 1px solid black;\n"
"	border-bottom: 1px solid black;\n"
"}")

        self.verticalLayout_4.addWidget(self.btn_page_3)

        self.btn_page_4 = QPushButton(self.frame_top_menus)
        self.btn_page_4.setObjectName(u"btn_page_4")
        self.btn_page_4.setMinimumSize(QSize(0, 40))
        self.btn_page_4.setFont(font1)
        self.btn_page_4.setStyleSheet(u"QPushButton {\n"
"	color: #616161;\n"
"	background-color: rgb(255, 255, 127);\n"
"	border: 0px solid;\n"
"}\n"
"QPushButton:hover {\n"
"	background-color: rgb(255, 255, 0);\n"
"	color: black;\n"
"	border-top: 1px solid black;\n"
"	border-bottom: 1px solid black;\n"
"}")

        self.verticalLayout_4.addWidget(self.btn_page_4)

        self.btn_page_5 = QPushButton(self.frame_top_menus)
        self.btn_page_5.setObjectName(u"btn_page_5")
        self.btn_page_5.setMinimumSize(QSize(0, 40))
        self.btn_page_5.setFont(font1)
        self.btn_page_5.setStyleSheet(u"QPushButton {\n"
"	color: #616161;\n"
"	background-color: rgb(255, 255, 127);\n"
"	border: 0px solid;\n"
"}\n"
"QPushButton:hover {\n"
"	background-color: rgb(255, 255, 0);\n"
"	color: black;\n"
"	border-top: 1px solid black;\n"
"	border-bottom: 1px solid black;\n"
"}")

        self.verticalLayout_4.addWidget(self.btn_page_5)


        self.verticalLayout_3.addWidget(self.frame_top_menus, 0, Qt.AlignTop)


        self.horizontalLayout_2.addWidget(self.frame_left_menu)

        self.frame_pages = QFrame(self.Content)
        self.frame_pages.setObjectName(u"frame_pages")
        self.frame_pages.setFrameShape(QFrame.StyledPanel)
        self.frame_pages.setFrameShadow(QFrame.Raised)
        self.verticalLayout_5 = QVBoxLayout(self.frame_pages)
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.stackedWidget = QStackedWidget(self.frame_pages)
        self.stackedWidget.setObjectName(u"stackedWidget")
        self.stackedWidget.setStyleSheet(u"background-color: rgb(241, 241, 241);")
        self.page_1 = QWidget()
        self.page_1.setObjectName(u"page_1")
        self.verticalLayout_7 = QVBoxLayout(self.page_1)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.frame_3 = QFrame(self.page_1)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setMaximumSize(QSize(16777215, 80))
        self.frame_3.setFrameShape(QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_5 = QHBoxLayout(self.frame_3)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.label_7 = QLabel(self.frame_3)
        self.label_7.setObjectName(u"label_7")
        font2 = QFont()
        font2.setPointSize(20)
        font2.setBold(True)
        self.label_7.setFont(font2)

        self.horizontalLayout_5.addWidget(self.label_7, 0, Qt.AlignHCenter)


        self.verticalLayout_7.addWidget(self.frame_3)

        self.frame = QFrame(self.page_1)
        self.frame.setObjectName(u"frame")
        self.frame.setMaximumSize(QSize(16777215, 228))
        self.frame.setFrameShape(QFrame.NoFrame)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_4 = QHBoxLayout(self.frame)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.groupBox = QGroupBox(self.frame)
        self.groupBox.setObjectName(u"groupBox")
        font3 = QFont()
        font3.setPointSize(12)
        font3.setBold(False)
        self.groupBox.setFont(font3)
        self.horizontalLayout_8 = QHBoxLayout(self.groupBox)
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_6 = QLabel(self.groupBox)
        self.label_6.setObjectName(u"label_6")
        font4 = QFont()
        font4.setPointSize(12)
        self.label_6.setFont(font4)
        self.label_6.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_6, 0, 0, 1, 1)

        self.lineEdit_id_med_item = QLineEdit(self.groupBox)
        self.lineEdit_id_med_item.setObjectName(u"lineEdit_id_med_item")
        self.lineEdit_id_med_item.setMinimumSize(QSize(0, 0))
        self.lineEdit_id_med_item.setMaximumSize(QSize(100, 16777215))
        self.lineEdit_id_med_item.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.gridLayout.addWidget(self.lineEdit_id_med_item, 0, 1, 1, 1)

        self.lineEdit_Desc_item = QLineEdit(self.groupBox)
        self.lineEdit_Desc_item.setObjectName(u"lineEdit_Desc_item")
        self.lineEdit_Desc_item.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.gridLayout.addWidget(self.lineEdit_Desc_item, 1, 1, 1, 3)

        self.label_4 = QLabel(self.groupBox)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setFont(font4)
        self.label_4.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_4, 2, 0, 1, 1)

        self.lineEdit_princp_item = QLineEdit(self.groupBox)
        self.lineEdit_princp_item.setObjectName(u"lineEdit_princp_item")
        self.lineEdit_princp_item.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.gridLayout.addWidget(self.lineEdit_princp_item, 2, 1, 1, 3)

        self.label_9 = QLabel(self.groupBox)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setFont(font4)
        self.label_9.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_9, 1, 0, 1, 1)

        self.lineEdit_alerta_item = QLineEdit(self.groupBox)
        self.lineEdit_alerta_item.setObjectName(u"lineEdit_alerta_item")
        self.lineEdit_alerta_item.setMaximumSize(QSize(500, 16777215))
        self.lineEdit_alerta_item.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.gridLayout.addWidget(self.lineEdit_alerta_item, 3, 1, 1, 1)

        self.label_10 = QLabel(self.groupBox)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setFont(font4)
        self.label_10.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_10, 3, 0, 1, 1)

        self.label_33 = QLabel(self.groupBox)
        self.label_33.setObjectName(u"label_33")

        self.gridLayout.addWidget(self.label_33, 0, 2, 1, 1)

        self.comboBox_Nome_Layout = QComboBox(self.groupBox)
        self.comboBox_Nome_Layout.setObjectName(u"comboBox_Nome_Layout")
        self.comboBox_Nome_Layout.setMinimumSize(QSize(300, 0))

        self.gridLayout.addWidget(self.comboBox_Nome_Layout, 0, 3, 1, 1)


        self.horizontalLayout_8.addLayout(self.gridLayout)


        self.horizontalLayout_4.addWidget(self.groupBox)


        self.verticalLayout_7.addWidget(self.frame)

        self.frame_2 = QFrame(self.page_1)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setMinimumSize(QSize(0, 80))
        self.frame_2.setMaximumSize(QSize(16777215, 80))
        self.frame_2.setFrameShape(QFrame.NoFrame)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_6 = QHBoxLayout(self.frame_2)
        self.horizontalLayout_6.setSpacing(0)
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.horizontalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.groupBox_2 = QGroupBox(self.frame_2)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.horizontalLayout_7 = QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.btn_localiza_item = QPushButton(self.groupBox_2)
        self.btn_localiza_item.setObjectName(u"btn_localiza_item")
        self.btn_localiza_item.setMinimumSize(QSize(0, 50))
        self.btn_localiza_item.setMaximumSize(QSize(100, 16777215))
        self.btn_localiza_item.setFont(font4)

        self.horizontalLayout_7.addWidget(self.btn_localiza_item)

        self.btn_cadastra_item = QPushButton(self.groupBox_2)
        self.btn_cadastra_item.setObjectName(u"btn_cadastra_item")
        self.btn_cadastra_item.setMinimumSize(QSize(0, 50))
        self.btn_cadastra_item.setMaximumSize(QSize(100, 16777215))
        self.btn_cadastra_item.setFont(font4)

        self.horizontalLayout_7.addWidget(self.btn_cadastra_item)

        self.btn_atualiza_item = QPushButton(self.groupBox_2)
        self.btn_atualiza_item.setObjectName(u"btn_atualiza_item")
        self.btn_atualiza_item.setMinimumSize(QSize(0, 50))
        self.btn_atualiza_item.setMaximumSize(QSize(100, 16777215))
        self.btn_atualiza_item.setFont(font4)

        self.horizontalLayout_7.addWidget(self.btn_atualiza_item)

        self.btn_exclui_item = QPushButton(self.groupBox_2)
        self.btn_exclui_item.setObjectName(u"btn_exclui_item")
        self.btn_exclui_item.setMinimumSize(QSize(0, 50))
        self.btn_exclui_item.setMaximumSize(QSize(100, 16777215))
        self.btn_exclui_item.setFont(font4)

        self.horizontalLayout_7.addWidget(self.btn_exclui_item)


        self.horizontalLayout_6.addWidget(self.groupBox_2)


        self.verticalLayout_7.addWidget(self.frame_2)

        self.stackedWidget.addWidget(self.page_1)
        self.page_2 = QWidget()
        self.page_2.setObjectName(u"page_2")
        self.verticalLayout_8 = QVBoxLayout(self.page_2)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.frame_7 = QFrame(self.page_2)
        self.frame_7.setObjectName(u"frame_7")
        self.frame_7.setMaximumSize(QSize(16777215, 60))
        self.frame_7.setFrameShape(QFrame.StyledPanel)
        self.frame_7.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_16 = QHBoxLayout(self.frame_7)
        self.horizontalLayout_16.setObjectName(u"horizontalLayout_16")
        self.label_22 = QLabel(self.frame_7)
        self.label_22.setObjectName(u"label_22")
        self.label_22.setFont(font2)

        self.horizontalLayout_16.addWidget(self.label_22, 0, Qt.AlignHCenter)


        self.verticalLayout_8.addWidget(self.frame_7)

        self.frame_12 = QFrame(self.page_2)
        self.frame_12.setObjectName(u"frame_12")
        self.frame_12.setFrameShape(QFrame.StyledPanel)
        self.frame_12.setFrameShadow(QFrame.Raised)
        self.gridLayout_6 = QGridLayout(self.frame_12)
        self.gridLayout_6.setSpacing(0)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.gridLayout_6.setContentsMargins(0, 0, 0, 0)
        self.frame_13 = QFrame(self.frame_12)
        self.frame_13.setObjectName(u"frame_13")
        self.frame_13.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.frame_13.setFrameShape(QFrame.StyledPanel)
        self.frame_13.setFrameShadow(QFrame.Raised)
        self.verticalLayout_11 = QVBoxLayout(self.frame_13)
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.lineEdit = QLineEdit(self.frame_13)
        self.lineEdit.setObjectName(u"lineEdit")
        self.lineEdit.setStyleSheet(u"border : 4px white;\n"
"background-color: rgb(255, 255, 255);")
        self.lineEdit.setAlignment(Qt.AlignCenter)

        self.verticalLayout_11.addWidget(self.lineEdit)

        self.lineEdit_2 = QLineEdit(self.frame_13)
        self.lineEdit_2.setObjectName(u"lineEdit_2")
        self.lineEdit_2.setMinimumSize(QSize(0, 0))
        self.lineEdit_2.setMaximumSize(QSize(500, 16777215))
        self.lineEdit_2.setStyleSheet(u"border : 4px white;\n"
"background-color: rgb(255, 255, 255);")
        self.lineEdit_2.setAlignment(Qt.AlignCenter)

        self.verticalLayout_11.addWidget(self.lineEdit_2)

        self.lineEdit_3 = QLineEdit(self.frame_13)
        self.lineEdit_3.setObjectName(u"lineEdit_3")
        self.lineEdit_3.setStyleSheet(u"border : 4px white;\n"
"background-color: rgb(255, 255, 255);")
        self.lineEdit_3.setAlignment(Qt.AlignCenter)

        self.verticalLayout_11.addWidget(self.lineEdit_3)

        self.lineEdit_4 = QLineEdit(self.frame_13)
        self.lineEdit_4.setObjectName(u"lineEdit_4")
        self.lineEdit_4.setStyleSheet(u"border : 4px white;\n"
"background-color: rgb(255, 255, 255);")
        self.lineEdit_4.setAlignment(Qt.AlignCenter)

        self.verticalLayout_11.addWidget(self.lineEdit_4)

        self.lineEdit_5 = QLineEdit(self.frame_13)
        self.lineEdit_5.setObjectName(u"lineEdit_5")
        self.lineEdit_5.setStyleSheet(u"border : 4px white;\n"
"background-color: rgb(255, 255, 255);")
        self.lineEdit_5.setAlignment(Qt.AlignCenter)

        self.verticalLayout_11.addWidget(self.lineEdit_5)

        self.lineEdit_6 = QLineEdit(self.frame_13)
        self.lineEdit_6.setObjectName(u"lineEdit_6")
        self.lineEdit_6.setStyleSheet(u"border : 4px white;\n"
"background-color: rgb(255, 255, 255);")
        self.lineEdit_6.setAlignment(Qt.AlignCenter)

        self.verticalLayout_11.addWidget(self.lineEdit_6)

        self.lineEdit_7 = QLineEdit(self.frame_13)
        self.lineEdit_7.setObjectName(u"lineEdit_7")
        self.lineEdit_7.setStyleSheet(u"border : 4px white;\n"
"background-color: rgb(255, 255, 255);")
        self.lineEdit_7.setAlignment(Qt.AlignCenter)

        self.verticalLayout_11.addWidget(self.lineEdit_7)

        self.frame_15 = QFrame(self.frame_13)
        self.frame_15.setObjectName(u"frame_15")
        self.frame_15.setFrameShape(QFrame.StyledPanel)
        self.frame_15.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_15 = QHBoxLayout(self.frame_15)
        self.horizontalLayout_15.setObjectName(u"horizontalLayout_15")
        self.label_5 = QLabel(self.frame_15)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setMaximumSize(QSize(150, 50))
        self.label_5.setPixmap(QPixmap(u"imgs/Ac\u0131badem_Grup_logo.svg.png"))
        self.label_5.setScaledContents(True)

        self.horizontalLayout_15.addWidget(self.label_5)

        self.label_26 = QLabel(self.frame_15)
        self.label_26.setObjectName(u"label_26")
        self.label_26.setMaximumSize(QSize(100, 100))
        self.label_26.setStyleSheet(u"border : 4px white;\n"
"background-color: rgb(255, 255, 255);")
        self.label_26.setPixmap(QPixmap(u":/imgs/imgs/datamatrix.JPG"))
        self.label_26.setScaledContents(True)
        self.label_26.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_15.addWidget(self.label_26)


        self.verticalLayout_11.addWidget(self.frame_15)

        self.lineEdit_9 = QLineEdit(self.frame_13)
        self.lineEdit_9.setObjectName(u"lineEdit_9")
        self.lineEdit_9.setMinimumSize(QSize(200, 0))
        self.lineEdit_9.setStyleSheet(u"border : 4px white;\n"
"background-color: rgb(255, 255, 255);")
        self.lineEdit_9.setAlignment(Qt.AlignCenter)

        self.verticalLayout_11.addWidget(self.lineEdit_9)


        self.gridLayout_6.addWidget(self.frame_13, 0, 1, 3, 1, Qt.AlignTop)

        self.groupBox_6 = QGroupBox(self.frame_12)
        self.groupBox_6.setObjectName(u"groupBox_6")
        font5 = QFont()
        font5.setBold(False)
        self.groupBox_6.setFont(font5)
        self.verticalLayout_12 = QVBoxLayout(self.groupBox_6)
        self.verticalLayout_12.setSpacing(0)
        self.verticalLayout_12.setObjectName(u"verticalLayout_12")
        self.verticalLayout_12.setContentsMargins(0, 0, 0, 0)
        self.frame_11 = QFrame(self.groupBox_6)
        self.frame_11.setObjectName(u"frame_11")
        self.frame_11.setFrameShape(QFrame.StyledPanel)
        self.frame_11.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_23 = QHBoxLayout(self.frame_11)
        self.horizontalLayout_23.setObjectName(u"horizontalLayout_23")
        self.gridLayout_5 = QGridLayout()
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.label_27 = QLabel(self.frame_11)
        self.label_27.setObjectName(u"label_27")
        self.label_27.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_5.addWidget(self.label_27, 0, 0, 1, 1)

        self.label_25 = QLabel(self.frame_11)
        self.label_25.setObjectName(u"label_25")
        self.label_25.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_5.addWidget(self.label_25, 2, 0, 1, 1)

        self.lineEdit_vence = QLineEdit(self.frame_11)
        self.lineEdit_vence.setObjectName(u"lineEdit_vence")
        self.lineEdit_vence.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.lineEdit_vence.setReadOnly(True)

        self.gridLayout_5.addWidget(self.lineEdit_vence, 2, 1, 1, 1)

        self.lineEdit_lote = QLineEdit(self.frame_11)
        self.lineEdit_lote.setObjectName(u"lineEdit_lote")
        self.lineEdit_lote.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.lineEdit_lote.setReadOnly(True)

        self.gridLayout_5.addWidget(self.lineEdit_lote, 1, 1, 1, 1)

        self.label_30 = QLabel(self.frame_11)
        self.label_30.setObjectName(u"label_30")
        self.label_30.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_5.addWidget(self.label_30, 1, 0, 1, 1)

        self.lineEdit_folio = QLineEdit(self.frame_11)
        self.lineEdit_folio.setObjectName(u"lineEdit_folio")
        self.lineEdit_folio.setMinimumSize(QSize(0, 0))
        self.lineEdit_folio.setMaximumSize(QSize(16777215, 16777215))
        self.lineEdit_folio.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.lineEdit_folio.setReadOnly(True)

        self.gridLayout_5.addWidget(self.lineEdit_folio, 0, 1, 1, 1)

        self.label_21 = QLabel(self.frame_11)
        self.label_21.setObjectName(u"label_21")

        self.gridLayout_5.addWidget(self.label_21, 3, 0, 1, 1)

        self.lineEdit_vence_gs1 = QLineEdit(self.frame_11)
        self.lineEdit_vence_gs1.setObjectName(u"lineEdit_vence_gs1")
        self.lineEdit_vence_gs1.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.lineEdit_vence_gs1.setReadOnly(True)

        self.gridLayout_5.addWidget(self.lineEdit_vence_gs1, 3, 1, 1, 1)


        self.horizontalLayout_23.addLayout(self.gridLayout_5)

        self.frame_14 = QFrame(self.frame_11)
        self.frame_14.setObjectName(u"frame_14")
        self.frame_14.setMinimumSize(QSize(130, 0))
        self.frame_14.setMaximumSize(QSize(100, 16777215))
        self.frame_14.setFrameShape(QFrame.StyledPanel)
        self.frame_14.setFrameShadow(QFrame.Raised)
        self.verticalLayout_14 = QVBoxLayout(self.frame_14)
        self.verticalLayout_14.setSpacing(0)
        self.verticalLayout_14.setObjectName(u"verticalLayout_14")
        self.verticalLayout_14.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_13 = QVBoxLayout()
        self.verticalLayout_13.setSpacing(5)
        self.verticalLayout_13.setObjectName(u"verticalLayout_13")
        self.verticalLayout_13.setSizeConstraint(QLayout.SetFixedSize)
        self.label_2 = QLabel(self.frame_14)
        self.label_2.setObjectName(u"label_2")
        font6 = QFont()
        font6.setBold(True)
        self.label_2.setFont(font6)

        self.verticalLayout_13.addWidget(self.label_2)

        self.comboBox = QComboBox(self.frame_14)
        self.comboBox.setObjectName(u"comboBox")
        self.comboBox.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.verticalLayout_13.addWidget(self.comboBox)

        self.lineEdit_senha_prod = QLineEdit(self.frame_14)
        self.lineEdit_senha_prod.setObjectName(u"lineEdit_senha_prod")
        self.lineEdit_senha_prod.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.lineEdit_senha_prod.setEchoMode(QLineEdit.Password)

        self.verticalLayout_13.addWidget(self.lineEdit_senha_prod)

        self.btn_envio_prod = QPushButton(self.frame_14)
        self.btn_envio_prod.setObjectName(u"btn_envio_prod")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.btn_envio_prod.sizePolicy().hasHeightForWidth())
        self.btn_envio_prod.setSizePolicy(sizePolicy2)
        self.btn_envio_prod.setMinimumSize(QSize(120, 0))
        self.btn_envio_prod.setMaximumSize(QSize(100, 16777215))

        self.verticalLayout_13.addWidget(self.btn_envio_prod, 0, Qt.AlignHCenter)


        self.verticalLayout_14.addLayout(self.verticalLayout_13)


        self.horizontalLayout_23.addWidget(self.frame_14)


        self.verticalLayout_12.addWidget(self.frame_11)


        self.gridLayout_6.addWidget(self.groupBox_6, 2, 0, 1, 1)

        self.groupBox_5 = QGroupBox(self.frame_12)
        self.groupBox_5.setObjectName(u"groupBox_5")
        self.groupBox_5.setFont(font5)
        self.horizontalLayout_14 = QHBoxLayout(self.groupBox_5)
        self.horizontalLayout_14.setSpacing(5)
        self.horizontalLayout_14.setObjectName(u"horizontalLayout_14")
        self.horizontalLayout_14.setContentsMargins(4, 0, 4, 0)
        self.gridLayout_3 = QGridLayout()
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_3.setVerticalSpacing(3)
        self.label_17 = QLabel(self.groupBox_5)
        self.label_17.setObjectName(u"label_17")
        self.label_17.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_3.addWidget(self.label_17, 2, 0, 1, 1)

        self.label_34 = QLabel(self.groupBox_5)
        self.label_34.setObjectName(u"label_34")
        self.label_34.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_3.addWidget(self.label_34, 4, 0, 1, 1)

        self.nome_layout = QLineEdit(self.groupBox_5)
        self.nome_layout.setObjectName(u"nome_layout")
        self.nome_layout.setReadOnly(True)

        self.gridLayout_3.addWidget(self.nome_layout, 4, 1, 1, 1)

        self.btn_localiza_Prod = QPushButton(self.groupBox_5)
        self.btn_localiza_Prod.setObjectName(u"btn_localiza_Prod")
        self.btn_localiza_Prod.setMinimumSize(QSize(130, 40))
        self.btn_localiza_Prod.setMaximumSize(QSize(100, 16777215))
        font7 = QFont()
        font7.setPointSize(11)
        self.btn_localiza_Prod.setFont(font7)

        self.gridLayout_3.addWidget(self.btn_localiza_Prod, 0, 2, 1, 1, Qt.AlignHCenter)

        self.lineEdit_Desc_Prod = QLineEdit(self.groupBox_5)
        self.lineEdit_Desc_Prod.setObjectName(u"lineEdit_Desc_Prod")
        self.lineEdit_Desc_Prod.setStyleSheet(u"background-color: rgb(248, 248, 248);")
        self.lineEdit_Desc_Prod.setReadOnly(True)

        self.gridLayout_3.addWidget(self.lineEdit_Desc_Prod, 1, 1, 1, 2)

        self.label_20 = QLabel(self.groupBox_5)
        self.label_20.setObjectName(u"label_20")
        self.label_20.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_3.addWidget(self.label_20, 1, 0, 1, 1)

        self.lineEdit_alerta_Prod = QLineEdit(self.groupBox_5)
        self.lineEdit_alerta_Prod.setObjectName(u"lineEdit_alerta_Prod")
        self.lineEdit_alerta_Prod.setMaximumSize(QSize(500, 16777215))
        self.lineEdit_alerta_Prod.setStyleSheet(u"background-color: rgb(248, 248, 248);")
        self.lineEdit_alerta_Prod.setReadOnly(True)

        self.gridLayout_3.addWidget(self.lineEdit_alerta_Prod, 3, 1, 1, 1)

        self.lineEdit_id_med_Prod = QLineEdit(self.groupBox_5)
        self.lineEdit_id_med_Prod.setObjectName(u"lineEdit_id_med_Prod")
        self.lineEdit_id_med_Prod.setMinimumSize(QSize(0, 0))
        self.lineEdit_id_med_Prod.setMaximumSize(QSize(135, 16777215))
        self.lineEdit_id_med_Prod.setStyleSheet(u"background-color: rgb(248, 248, 248);")
        self.lineEdit_id_med_Prod.setReadOnly(True)

        self.gridLayout_3.addWidget(self.lineEdit_id_med_Prod, 0, 1, 1, 1)

        self.label_18 = QLabel(self.groupBox_5)
        self.label_18.setObjectName(u"label_18")
        self.label_18.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_3.addWidget(self.label_18, 3, 0, 1, 1)

        self.label_19 = QLabel(self.groupBox_5)
        self.label_19.setObjectName(u"label_19")
        self.label_19.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_3.addWidget(self.label_19, 0, 0, 1, 1)

        self.lineEdit_princp_Prod = QLineEdit(self.groupBox_5)
        self.lineEdit_princp_Prod.setObjectName(u"lineEdit_princp_Prod")
        self.lineEdit_princp_Prod.setStyleSheet(u"background-color: rgb(248, 248, 248);")
        self.lineEdit_princp_Prod.setReadOnly(True)

        self.gridLayout_3.addWidget(self.lineEdit_princp_Prod, 2, 1, 1, 2)


        self.horizontalLayout_14.addLayout(self.gridLayout_3)


        self.gridLayout_6.addWidget(self.groupBox_5, 0, 0, 2, 1)


        self.verticalLayout_8.addWidget(self.frame_12)

        self.stackedWidget.addWidget(self.page_2)
        self.page_3 = QWidget()
        self.page_3.setObjectName(u"page_3")
        self.verticalLayout_6 = QVBoxLayout(self.page_3)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.frame_8 = QFrame(self.page_3)
        self.frame_8.setObjectName(u"frame_8")
        self.frame_8.setMaximumSize(QSize(16777215, 80))
        self.frame_8.setFrameShape(QFrame.StyledPanel)
        self.frame_8.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_18 = QHBoxLayout(self.frame_8)
        self.horizontalLayout_18.setObjectName(u"horizontalLayout_18")
        self.label_28 = QLabel(self.frame_8)
        self.label_28.setObjectName(u"label_28")
        self.label_28.setFont(font2)

        self.horizontalLayout_18.addWidget(self.label_28, 0, Qt.AlignHCenter)


        self.verticalLayout_6.addWidget(self.frame_8)

        self.groupBox_7 = QGroupBox(self.page_3)
        self.groupBox_7.setObjectName(u"groupBox_7")
        self.groupBox_7.setFont(font1)
        self.horizontalLayout_17 = QHBoxLayout(self.groupBox_7)
        self.horizontalLayout_17.setObjectName(u"horizontalLayout_17")
        self.tabela_historico = QTableWidget(self.groupBox_7)
        self.tabela_historico.setObjectName(u"tabela_historico")
        self.tabela_historico.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.horizontalLayout_17.addWidget(self.tabela_historico)


        self.verticalLayout_6.addWidget(self.groupBox_7)

        self.frame_10 = QFrame(self.page_3)
        self.frame_10.setObjectName(u"frame_10")
        self.frame_10.setMinimumSize(QSize(0, 100))
        self.frame_10.setFrameShape(QFrame.StyledPanel)
        self.frame_10.setFrameShadow(QFrame.Raised)
        self.verticalLayout_15 = QVBoxLayout(self.frame_10)
        self.verticalLayout_15.setObjectName(u"verticalLayout_15")
        self.label_35 = QLabel(self.frame_10)
        self.label_35.setObjectName(u"label_35")

        self.verticalLayout_15.addWidget(self.label_35)

        self.btn_exp_excell = QPushButton(self.frame_10)
        self.btn_exp_excell.setObjectName(u"btn_exp_excell")

        self.verticalLayout_15.addWidget(self.btn_exp_excell)


        self.verticalLayout_6.addWidget(self.frame_10)

        self.stackedWidget.addWidget(self.page_3)
        self.page_4 = QWidget()
        self.page_4.setObjectName(u"page_4")
        self.verticalLayout_9 = QVBoxLayout(self.page_4)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.frame_6 = QFrame(self.page_4)
        self.frame_6.setObjectName(u"frame_6")
        self.frame_6.setMaximumSize(QSize(16777215, 80))
        self.frame_6.setFrameShape(QFrame.StyledPanel)
        self.frame_6.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_13 = QHBoxLayout(self.frame_6)
        self.horizontalLayout_13.setObjectName(u"horizontalLayout_13")
        self.label_16 = QLabel(self.frame_6)
        self.label_16.setObjectName(u"label_16")
        self.label_16.setFont(font2)

        self.horizontalLayout_13.addWidget(self.label_16, 0, Qt.AlignHCenter)


        self.verticalLayout_9.addWidget(self.frame_6)

        self.frame_4 = QFrame(self.page_4)
        self.frame_4.setObjectName(u"frame_4")
        self.frame_4.setFrameShape(QFrame.NoFrame)
        self.frame_4.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_9 = QHBoxLayout(self.frame_4)
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.groupBox_3 = QGroupBox(self.frame_4)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.horizontalLayout_10 = QHBoxLayout(self.groupBox_3)
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.label_12 = QLabel(self.groupBox_3)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setFont(font4)
        self.label_12.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_12, 3, 0, 1, 1)

        self.label_14 = QLabel(self.groupBox_3)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setFont(font4)
        self.label_14.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_14, 2, 2, 1, 1)

        self.label_11 = QLabel(self.groupBox_3)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setFont(font4)
        self.label_11.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_11, 2, 0, 1, 1)

        self.lineEdit_ABV_usuario = QLineEdit(self.groupBox_3)
        self.lineEdit_ABV_usuario.setObjectName(u"lineEdit_ABV_usuario")
        self.lineEdit_ABV_usuario.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.gridLayout_2.addWidget(self.lineEdit_ABV_usuario, 2, 1, 1, 1)

        self.lineEdit_cargo_usuario = QLineEdit(self.groupBox_3)
        self.lineEdit_cargo_usuario.setObjectName(u"lineEdit_cargo_usuario")
        self.lineEdit_cargo_usuario.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.gridLayout_2.addWidget(self.lineEdit_cargo_usuario, 2, 3, 1, 1)

        self.label_13 = QLabel(self.groupBox_3)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setFont(font4)
        self.label_13.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_13, 1, 0, 1, 1)

        self.lineEdit_SenhaConfirm = QLineEdit(self.groupBox_3)
        self.lineEdit_SenhaConfirm.setObjectName(u"lineEdit_SenhaConfirm")
        self.lineEdit_SenhaConfirm.setMaximumSize(QSize(16777215, 16777215))
        self.lineEdit_SenhaConfirm.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.lineEdit_SenhaConfirm.setEchoMode(QLineEdit.Password)

        self.gridLayout_2.addWidget(self.lineEdit_SenhaConfirm, 3, 3, 1, 1)

        self.lineEdit_Nome_usuario = QLineEdit(self.groupBox_3)
        self.lineEdit_Nome_usuario.setObjectName(u"lineEdit_Nome_usuario")
        self.lineEdit_Nome_usuario.setMinimumSize(QSize(0, 0))
        self.lineEdit_Nome_usuario.setMaximumSize(QSize(16777215, 16777215))
        self.lineEdit_Nome_usuario.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.gridLayout_2.addWidget(self.lineEdit_Nome_usuario, 1, 1, 1, 4)

        self.lineEdit_senha_usuario = QLineEdit(self.groupBox_3)
        self.lineEdit_senha_usuario.setObjectName(u"lineEdit_senha_usuario")
        self.lineEdit_senha_usuario.setMaximumSize(QSize(16777215, 16777215))
        self.lineEdit_senha_usuario.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.lineEdit_senha_usuario.setEchoMode(QLineEdit.Password)

        self.gridLayout_2.addWidget(self.lineEdit_senha_usuario, 3, 1, 1, 1)

        self.label_15 = QLabel(self.groupBox_3)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setFont(font4)
        self.label_15.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_15, 3, 2, 1, 1)

        self.label_31 = QLabel(self.groupBox_3)
        self.label_31.setObjectName(u"label_31")
        self.label_31.setFont(font4)

        self.gridLayout_2.addWidget(self.label_31, 0, 0, 1, 1, Qt.AlignRight)

        self.cbb_Nivel = QComboBox(self.groupBox_3)
        self.cbb_Nivel.addItem("")
        self.cbb_Nivel.addItem("")
        self.cbb_Nivel.setObjectName(u"cbb_Nivel")
        self.cbb_Nivel.setMaximumSize(QSize(60, 16777215))

        self.gridLayout_2.addWidget(self.cbb_Nivel, 0, 1, 1, 1)


        self.horizontalLayout_10.addLayout(self.gridLayout_2)


        self.horizontalLayout_9.addWidget(self.groupBox_3)


        self.verticalLayout_9.addWidget(self.frame_4)

        self.frame_5 = QFrame(self.page_4)
        self.frame_5.setObjectName(u"frame_5")
        self.frame_5.setMinimumSize(QSize(0, 80))
        self.frame_5.setMaximumSize(QSize(16777215, 80))
        self.frame_5.setFrameShape(QFrame.NoFrame)
        self.frame_5.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_11 = QHBoxLayout(self.frame_5)
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.groupBox_4 = QGroupBox(self.frame_5)
        self.groupBox_4.setObjectName(u"groupBox_4")
        self.horizontalLayout_12 = QHBoxLayout(self.groupBox_4)
        self.horizontalLayout_12.setObjectName(u"horizontalLayout_12")
        self.btn_localiza_Usuario = QPushButton(self.groupBox_4)
        self.btn_localiza_Usuario.setObjectName(u"btn_localiza_Usuario")
        self.btn_localiza_Usuario.setMinimumSize(QSize(0, 40))
        self.btn_localiza_Usuario.setMaximumSize(QSize(140, 16777215))
        self.btn_localiza_Usuario.setFont(font4)

        self.horizontalLayout_12.addWidget(self.btn_localiza_Usuario)

        self.btn_CadastrarUsuario = QPushButton(self.groupBox_4)
        self.btn_CadastrarUsuario.setObjectName(u"btn_CadastrarUsuario")
        self.btn_CadastrarUsuario.setMinimumSize(QSize(0, 40))
        self.btn_CadastrarUsuario.setMaximumSize(QSize(100, 16777215))
        self.btn_CadastrarUsuario.setFont(font4)

        self.horizontalLayout_12.addWidget(self.btn_CadastrarUsuario)

        self.btn_ExcluirCadastro = QPushButton(self.groupBox_4)
        self.btn_ExcluirCadastro.setObjectName(u"btn_ExcluirCadastro")
        self.btn_ExcluirCadastro.setMinimumSize(QSize(0, 40))
        self.btn_ExcluirCadastro.setMaximumSize(QSize(80, 16777215))
        self.btn_ExcluirCadastro.setFont(font4)

        self.horizontalLayout_12.addWidget(self.btn_ExcluirCadastro)

        self.label = QLabel(self.groupBox_4)
        self.label.setObjectName(u"label")
        self.label.setFont(font4)

        self.horizontalLayout_12.addWidget(self.label, 0, Qt.AlignRight)

        self.txt_SenhaAntiga = QLineEdit(self.groupBox_4)
        self.txt_SenhaAntiga.setObjectName(u"txt_SenhaAntiga")
        self.txt_SenhaAntiga.setMaximumSize(QSize(100, 16777215))
        self.txt_SenhaAntiga.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.txt_SenhaAntiga.setEchoMode(QLineEdit.Password)

        self.horizontalLayout_12.addWidget(self.txt_SenhaAntiga)

        self.btn_AtualizarCadastro = QPushButton(self.groupBox_4)
        self.btn_AtualizarCadastro.setObjectName(u"btn_AtualizarCadastro")
        self.btn_AtualizarCadastro.setMinimumSize(QSize(0, 40))
        self.btn_AtualizarCadastro.setMaximumSize(QSize(140, 16777215))
        self.btn_AtualizarCadastro.setFont(font4)

        self.horizontalLayout_12.addWidget(self.btn_AtualizarCadastro)


        self.horizontalLayout_11.addWidget(self.groupBox_4)


        self.verticalLayout_9.addWidget(self.frame_5)

        self.stackedWidget.addWidget(self.page_4)
        self.page_5 = QWidget()
        self.page_5.setObjectName(u"page_5")
        self.verticalLayout_10 = QVBoxLayout(self.page_5)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.frame_9 = QFrame(self.page_5)
        self.frame_9.setObjectName(u"frame_9")
        self.frame_9.setMaximumSize(QSize(16777215, 80))
        self.frame_9.setFrameShape(QFrame.StyledPanel)
        self.frame_9.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_22 = QHBoxLayout(self.frame_9)
        self.horizontalLayout_22.setObjectName(u"horizontalLayout_22")
        self.label_29 = QLabel(self.frame_9)
        self.label_29.setObjectName(u"label_29")
        self.label_29.setFont(font2)

        self.horizontalLayout_22.addWidget(self.label_29, 0, Qt.AlignHCenter)


        self.verticalLayout_10.addWidget(self.frame_9)

        self.groupBox_9 = QGroupBox(self.page_5)
        self.groupBox_9.setObjectName(u"groupBox_9")
        self.horizontalLayout_20 = QHBoxLayout(self.groupBox_9)
        self.horizontalLayout_20.setObjectName(u"horizontalLayout_20")
        self.gridLayout_4 = QGridLayout()
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.horizontalSpacer_7 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_4.addItem(self.horizontalSpacer_7, 0, 4, 1, 1)

        self.label_24 = QLabel(self.groupBox_9)
        self.label_24.setObjectName(u"label_24")
        self.label_24.setFont(font4)
        self.label_24.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_4.addWidget(self.label_24, 1, 0, 1, 1, Qt.AlignLeft)

        self.lineEdit_Porta = QLineEdit(self.groupBox_9)
        self.lineEdit_Porta.setObjectName(u"lineEdit_Porta")
        self.lineEdit_Porta.setMaximumSize(QSize(500, 16777215))
        self.lineEdit_Porta.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.gridLayout_4.addWidget(self.lineEdit_Porta, 1, 1, 1, 1)

        self.label_32 = QLabel(self.groupBox_9)
        self.label_32.setObjectName(u"label_32")
        self.label_32.setFont(font4)

        self.gridLayout_4.addWidget(self.label_32, 2, 0, 1, 1)

        self.lineEdit_IP = QLineEdit(self.groupBox_9)
        self.lineEdit_IP.setObjectName(u"lineEdit_IP")
        self.lineEdit_IP.setMinimumSize(QSize(0, 0))
        self.lineEdit_IP.setMaximumSize(QSize(500, 16777215))
        self.lineEdit_IP.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.gridLayout_4.addWidget(self.lineEdit_IP, 0, 1, 1, 1)

        self.horizontalSpacer_8 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_4.addItem(self.horizontalSpacer_8, 0, 3, 1, 1)

        self.label_23 = QLabel(self.groupBox_9)
        self.label_23.setObjectName(u"label_23")
        self.label_23.setFont(font4)
        self.label_23.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_4.addWidget(self.label_23, 0, 0, 1, 1, Qt.AlignLeft)

        self.horizontalSpacer_9 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_4.addItem(self.horizontalSpacer_9, 0, 2, 1, 1)

        self.cbb_Idioma = QComboBox(self.groupBox_9)
        self.cbb_Idioma.addItem("")
        self.cbb_Idioma.addItem("")
        self.cbb_Idioma.addItem("")
        self.cbb_Idioma.addItem("")
        self.cbb_Idioma.setObjectName(u"cbb_Idioma")

        self.gridLayout_4.addWidget(self.cbb_Idioma, 2, 1, 1, 1)


        self.horizontalLayout_20.addLayout(self.gridLayout_4)


        self.verticalLayout_10.addWidget(self.groupBox_9)

        self.groupBox_8 = QGroupBox(self.page_5)
        self.groupBox_8.setObjectName(u"groupBox_8")
        self.cmb_cad_layout = QComboBox(self.groupBox_8)
        self.cmb_cad_layout.setObjectName(u"cmb_cad_layout")
        self.cmb_cad_layout.setGeometry(QRect(20, 30, 231, 22))
        self.btn_delete_layout = QPushButton(self.groupBox_8)
        self.btn_delete_layout.setObjectName(u"btn_delete_layout")
        self.btn_delete_layout.setGeometry(QRect(20, 70, 75, 23))
        self.linedit_add_layout = QLineEdit(self.groupBox_8)
        self.linedit_add_layout.setObjectName(u"linedit_add_layout")
        self.linedit_add_layout.setGeometry(QRect(300, 30, 113, 20))
        self.linedit_add_layout.setStyleSheet(u"background-color: rgb(255, 255, 255);")
        self.btn_add_layout = QPushButton(self.groupBox_8)
        self.btn_add_layout.setObjectName(u"btn_add_layout")
        self.btn_add_layout.setGeometry(QRect(300, 60, 75, 23))

        self.verticalLayout_10.addWidget(self.groupBox_8)

        self.groupBox_10 = QGroupBox(self.page_5)
        self.groupBox_10.setObjectName(u"groupBox_10")
        self.horizontalLayout_21 = QHBoxLayout(self.groupBox_10)
        self.horizontalLayout_21.setObjectName(u"horizontalLayout_21")
        self.btn_Atualiza_Config = QPushButton(self.groupBox_10)
        self.btn_Atualiza_Config.setObjectName(u"btn_Atualiza_Config")
        self.btn_Atualiza_Config.setMinimumSize(QSize(0, 60))
        self.btn_Atualiza_Config.setMaximumSize(QSize(100, 16777215))
        self.btn_Atualiza_Config.setFont(font1)

        self.horizontalLayout_21.addWidget(self.btn_Atualiza_Config)


        self.verticalLayout_10.addWidget(self.groupBox_10)

        self.stackedWidget.addWidget(self.page_5)

        self.verticalLayout_5.addWidget(self.stackedWidget)


        self.horizontalLayout_2.addWidget(self.frame_pages)


        self.verticalLayout.addWidget(self.Content)

        MainWindow.setCentralWidget(self.centralwidget)
        QWidget.setTabOrder(self.lineEdit_id_med_item, self.lineEdit_Desc_item)
        QWidget.setTabOrder(self.lineEdit_Desc_item, self.lineEdit_princp_item)
        QWidget.setTabOrder(self.lineEdit_princp_item, self.btn_localiza_item)
        QWidget.setTabOrder(self.btn_localiza_item, self.btn_cadastra_item)
        QWidget.setTabOrder(self.btn_cadastra_item, self.btn_atualiza_item)
        QWidget.setTabOrder(self.btn_atualiza_item, self.btn_exclui_item)
        QWidget.setTabOrder(self.btn_exclui_item, self.btn_page_1)
        QWidget.setTabOrder(self.btn_page_1, self.btn_page_2)
        QWidget.setTabOrder(self.btn_page_2, self.btn_page_3)
        QWidget.setTabOrder(self.btn_page_3, self.btn_page_4)
        QWidget.setTabOrder(self.btn_page_4, self.lineEdit_Nome_usuario)
        QWidget.setTabOrder(self.lineEdit_Nome_usuario, self.lineEdit_ABV_usuario)
        QWidget.setTabOrder(self.lineEdit_ABV_usuario, self.lineEdit_cargo_usuario)
        QWidget.setTabOrder(self.lineEdit_cargo_usuario, self.lineEdit_senha_usuario)
        QWidget.setTabOrder(self.lineEdit_senha_usuario, self.lineEdit_SenhaConfirm)
        QWidget.setTabOrder(self.lineEdit_SenhaConfirm, self.btn_localiza_Usuario)
        QWidget.setTabOrder(self.btn_localiza_Usuario, self.btn_CadastrarUsuario)
        QWidget.setTabOrder(self.btn_CadastrarUsuario, self.btn_AtualizarCadastro)

        self.retranslateUi(MainWindow)

        self.stackedWidget.setCurrentIndex(1)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Mea Modul", None))
        self.label_8.setText("")
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Integra\u00e7\u00e3o Mea Modul <-> TriaTech", None))
        self.btn_page_1.setText(QCoreApplication.translate("MainWindow", u"Item", None))
        self.btn_page_2.setText(QCoreApplication.translate("MainWindow", u"Produ\u00e7\u00e3o", None))
        self.btn_page_3.setText(QCoreApplication.translate("MainWindow", u"Hist\u00f3rico", None))
        self.btn_page_4.setText(QCoreApplication.translate("MainWindow", u"Usu\u00e1rios", None))
        self.btn_page_5.setText(QCoreApplication.translate("MainWindow", u"Configura\u00e7\u00e3o", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"Cadastro Itens", None))
        self.groupBox.setTitle(QCoreApplication.translate("MainWindow", u"Cadastro de Itens", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"C\u00f3digo", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Principio Ativo", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"Descri\u00e7\u00e3o", None))
        self.label_10.setText(QCoreApplication.translate("MainWindow", u"C\u00f3digo de Barras", None))
        self.label_33.setText(QCoreApplication.translate("MainWindow", u"Layout", None))
        self.groupBox_2.setTitle("")
        self.btn_localiza_item.setText(QCoreApplication.translate("MainWindow", u"Localizar", None))
        self.btn_cadastra_item.setText(QCoreApplication.translate("MainWindow", u"Cadastrar", None))
        self.btn_atualiza_item.setText(QCoreApplication.translate("MainWindow", u"Atualizar", None))
        self.btn_exclui_item.setText(QCoreApplication.translate("MainWindow", u"Excluir", None))
        self.label_22.setText(QCoreApplication.translate("MainWindow", u"Produ\u00e7\u00e3o", None))
        self.label_5.setText("")
        self.label_26.setText("")
        self.lineEdit_9.setText(QCoreApplication.translate("MainWindow", u"ACIBADEM", None))
        self.groupBox_6.setTitle(QCoreApplication.translate("MainWindow", u"Campos de Produ\u00e7\u00e3o", None))
        self.label_27.setText(QCoreApplication.translate("MainWindow", u"Serial", None))
        self.label_25.setText(QCoreApplication.translate("MainWindow", u"Validade", None))
        self.label_30.setText(QCoreApplication.translate("MainWindow", u"Lote", None))
        self.label_21.setText(QCoreApplication.translate("MainWindow", u"Validade (GS1)", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Aprova\u00e7\u00e3o", None))
        self.btn_envio_prod.setText(QCoreApplication.translate("MainWindow", u"Enviar", None))
        self.groupBox_5.setTitle(QCoreApplication.translate("MainWindow", u"Pesquisa de Item", None))
        self.label_17.setText(QCoreApplication.translate("MainWindow", u"Principio Ativo", None))
        self.label_34.setText(QCoreApplication.translate("MainWindow", u"Layout", None))
        self.btn_localiza_Prod.setText(QCoreApplication.translate("MainWindow", u"Localizar", None))
        self.label_20.setText(QCoreApplication.translate("MainWindow", u"Descri\u00e7\u00e3o", None))
        self.label_18.setText(QCoreApplication.translate("MainWindow", u"C\u00f3digo de barras", None))
        self.label_19.setText(QCoreApplication.translate("MainWindow", u"C\u00f3digo", None))
        self.label_28.setText(QCoreApplication.translate("MainWindow", u"Hist\u00f3rico Produ\u00e7\u00e3o", None))
        self.groupBox_7.setTitle(QCoreApplication.translate("MainWindow", u"Hist\u00f3rico", None))
        self.label_35.setText(QCoreApplication.translate("MainWindow", u"Export", None))
        self.btn_exp_excell.setText(QCoreApplication.translate("MainWindow", u"Export Excel", None))
        self.label_16.setText(QCoreApplication.translate("MainWindow", u"Cadastro Usu\u00e1rios", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("MainWindow", u"Usu\u00e1rio", None))
        self.label_12.setText(QCoreApplication.translate("MainWindow", u"Senha", None))
        self.label_14.setText(QCoreApplication.translate("MainWindow", u"Cargo", None))
        self.label_11.setText(QCoreApplication.translate("MainWindow", u"Abreviatura", None))
        self.label_13.setText(QCoreApplication.translate("MainWindow", u"Nome", None))
        self.label_15.setText(QCoreApplication.translate("MainWindow", u"Senha", None))
        self.label_31.setText(QCoreApplication.translate("MainWindow", u"N\u00edvel", None))
        self.cbb_Nivel.setItemText(0, QCoreApplication.translate("MainWindow", u"OPE", None))
        self.cbb_Nivel.setItemText(1, QCoreApplication.translate("MainWindow", u"ADM", None))

        self.groupBox_4.setTitle("")
        self.btn_localiza_Usuario.setText(QCoreApplication.translate("MainWindow", u"Localizar cadastro", None))
        self.btn_CadastrarUsuario.setText(QCoreApplication.translate("MainWindow", u"Cadastrar", None))
        self.btn_ExcluirCadastro.setText(QCoreApplication.translate("MainWindow", u"Excluir", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"Senha atual:", None))
        self.btn_AtualizarCadastro.setText(QCoreApplication.translate("MainWindow", u"Atualizar Cadastro", None))
        self.label_29.setText(QCoreApplication.translate("MainWindow", u"Configura\u00e7\u00e3o", None))
        self.groupBox_9.setTitle(QCoreApplication.translate("MainWindow", u"Configura\u00e7\u00e3o", None))
        self.label_24.setText(QCoreApplication.translate("MainWindow", u"Porta", None))
        self.label_32.setText(QCoreApplication.translate("MainWindow", u"Idioma", None))
        self.label_23.setText(QCoreApplication.translate("MainWindow", u"IP", None))
        self.cbb_Idioma.setItemText(0, QCoreApplication.translate("MainWindow", u"Portugu\u00eas - Brasil", None))
        self.cbb_Idioma.setItemText(1, QCoreApplication.translate("MainWindow", u"English", None))
        self.cbb_Idioma.setItemText(2, QCoreApplication.translate("MainWindow", u"Espa\u00f1ol", None))
        self.cbb_Idioma.setItemText(3, QCoreApplication.translate("MainWindow", u"Turkish", None))

        self.groupBox_8.setTitle(QCoreApplication.translate("MainWindow", u"Layouts", None))
        self.btn_delete_layout.setText(QCoreApplication.translate("MainWindow", u"Apagar", None))
        self.btn_add_layout.setText(QCoreApplication.translate("MainWindow", u"Adicionar", None))
        self.groupBox_10.setTitle("")
        self.btn_Atualiza_Config.setText(QCoreApplication.translate("MainWindow", u"Atualizar", None))
    # retranslateUi

