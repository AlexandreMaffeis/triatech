# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'BuscarMed_LeitorKbEHps.ui'
##
## Created by: Qt User Interface Compiler version 6.6.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QDialog, QFrame, QHBoxLayout,
    QLabel, QPushButton, QSizePolicy, QVBoxLayout,
    QWidget)

class Ui_Dialog_leitor(object):
    def setupUi(self, Dialog_leitor):
        if not Dialog_leitor.objectName():
            Dialog_leitor.setObjectName(u"Dialog_leitor")
        Dialog_leitor.resize(616, 236)
        Dialog_leitor.setMinimumSize(QSize(0, 0))
        self.verticalLayout = QVBoxLayout(Dialog_leitor)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.frame_2 = QFrame(Dialog_leitor)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setMaximumSize(QSize(16777215, 80))
        self.frame_2.setStyleSheet(u"background-color:#0056a8;\n"
"color:white;")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.frame_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label = QLabel(self.frame_2)
        self.label.setObjectName(u"label")
        font = QFont()
        font.setFamilies([u"MS Sans Serif"])
        font.setPointSize(35)
        self.label.setFont(font)

        self.verticalLayout_2.addWidget(self.label, 0, Qt.AlignHCenter)


        self.verticalLayout.addWidget(self.frame_2)

        self.frame = QFrame(Dialog_leitor)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_3 = QVBoxLayout(self.frame)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.frame_3 = QFrame(self.frame)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setFrameShape(QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.frame_3)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.label_Buffer = QLabel(self.frame_3)
        self.label_Buffer.setObjectName(u"label_Buffer")

        self.verticalLayout_4.addWidget(self.label_Buffer)


        self.verticalLayout_3.addWidget(self.frame_3)

        self.frame_4 = QFrame(self.frame)
        self.frame_4.setObjectName(u"frame_4")
        self.frame_4.setFrameShape(QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame_4)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.btn_Selecionar = QPushButton(self.frame_4)
        self.btn_Selecionar.setObjectName(u"btn_Selecionar")
        self.btn_Selecionar.setMinimumSize(QSize(100, 40))
        font1 = QFont()
        font1.setPointSize(12)
        self.btn_Selecionar.setFont(font1)

        self.horizontalLayout.addWidget(self.btn_Selecionar)

        self.btn_Cancelar = QPushButton(self.frame_4)
        self.btn_Cancelar.setObjectName(u"btn_Cancelar")
        self.btn_Cancelar.setMinimumSize(QSize(100, 40))
        self.btn_Cancelar.setFont(font1)

        self.horizontalLayout.addWidget(self.btn_Cancelar)


        self.verticalLayout_3.addWidget(self.frame_4, 0, Qt.AlignRight)


        self.verticalLayout.addWidget(self.frame)


        self.retranslateUi(Dialog_leitor)

        QMetaObject.connectSlotsByName(Dialog_leitor)
    # setupUi

    def retranslateUi(self, Dialog_leitor):
        Dialog_leitor.setWindowTitle(QCoreApplication.translate("Dialog_leitor", u"Dialog", None))
        self.label.setText(QCoreApplication.translate("Dialog_leitor", u"Pesquisa de medicamentos", None))
        self.label_Buffer.setText(QCoreApplication.translate("Dialog_leitor", u"Leitura:", None))
        self.btn_Selecionar.setText(QCoreApplication.translate("Dialog_leitor", u"Selecionar", None))
        self.btn_Cancelar.setText(QCoreApplication.translate("Dialog_leitor", u"Cancelar", None))
    # retranslateUi

