import sqlite3

class Comunic_Banco():


#Tabela Config
    def Le_tabela_config(self):
        conn = sqlite3.connect('bd_integracao.db')
        print ("banco conectado")
        cursor = conn.cursor()
        
        cursor.execute("""select * from config2""")
        valores = cursor.fetchall()

        return valores
        conn.close()

    def Atualizar_config_banco (self, ip, porta, idioma, item):
        print("Pesquisa no Banco")
        conn = sqlite3.connect('bd_integracao.db')
        print ("banco conectado")
        cursor = conn.cursor()

        cursor.execute("""UPDATE config2 SET IP = ?, Porta = ?, idioma = ? WHERE ID = ?""",(ip, porta, idioma, item))
        
        conn.commit()
        conn.close()

#tabela Medicamento
    def Le_tabela_medicamento(self):
        conn = sqlite3.connect('bd_integracao.db')
        print ("banco conectado")
        cursor = conn.cursor()
        
        cursor.execute("""select * from medicamentos""")
        teste = cursor.fetchall()

        return teste
        conn.close()

    def pesquisa_item_banco (self, item_pesquisar):
        conn = sqlite3.connect('bd_integracao.db')
        print ("banco conectado")
        cursor = conn.cursor()
        
        cursor.execute("""select * from medicamentos WHERE barcode = ?""", (item_pesquisar,))
        teste = cursor.fetchall()

        return teste

        conn.close()

    def cadastrar_item_banco (self, id_medicamento,barcode,descricao_medicamento,principio_ativo,layout):
        conn = sqlite3.connect('bd_integracao.db')
        print ("banco conectado")
        cursor = conn.cursor()
        
        cursor.execute("""
        INSERT INTO medicamentos (id_medicamento,barcode,descricao_medicamento,principio_ativo,layout)
        VALUES (?, ?, ?, ?, ?)""",(id_medicamento,barcode,descricao_medicamento,principio_ativo,layout))

        conn.commit()
        conn.close()

    def Atualizar_item_banco (self, id_medicamento,barcode,descricao_medicamento,principio_ativo,layout):
        conn = sqlite3.connect('bd_integracao.db')
        print ("banco conectado")
        cursor = conn.cursor()

        cursor.execute("""UPDATE medicamentos SET barcode = ?, descricao_medicamento = ?, principio_ativo = ?, layout = ? WHERE id_medicamento = ?""",
                       (barcode,descricao_medicamento,principio_ativo,layout, id_medicamento))
         
        conn.commit()
        conn.close()


    def deletar_item_banco (self, item):
            conn = sqlite3.connect('bd_integracao.db')
            print ("banco conectado")
            cursor = conn.cursor()
            cursor.execute("""DELETE FROM medicamentos WHERE id_medicamento= ? """,(item,))
            conn.commit()
            conn.close()

#tabela Produção
    def Le_tabela_producao(self):
        conn = sqlite3.connect('bd_integracao.db')
        print ("banco conectado")
        cursor = conn.cursor()
        
        cursor.execute("""SELECT * FROM producao INNER JOIN medicamentos ON producao.produto = medicamentos.id""")
        teste = cursor.fetchall()
        conn.close()
        return teste


    def cadastrar_Producao_banco (self, produto, folio, lote, vence, aprovacao, data, hora):
        print("Pesquisa no Banco")

        conn = sqlite3.connect('bd_integracao.db')
        print ("banco conectado")
        cursor = conn.cursor()
        
        cursor.execute("""
        INSERT INTO producao (produto, folio, lote, vence, aprovacao, data, hora)
        VALUES (?, ?, ?, ?, ?, ?, ?)""",(produto, folio, lote, vence, aprovacao, data, hora))

        conn.commit()
        conn.close()


    def pesquisa_usuario_banco (self, item_pesquisar):
        print("Pesquins no Banco")

        conn = sqlite3.connect('bd_integracao.db')
        print ("banco conectado")
        cursor = conn.cursor()
        
        cursor.execute("""select * from usuario WHERE nome = ?""", (item_pesquisar,))
        teste = cursor.fetchall()

        return teste

        conn.close()

    def pesquisa_ABV_banco (self, item_pesquisar):
        print("Pesquins no Banco")

        conn = sqlite3.connect('bd_integracao.db')
        print ("banco conectado")
        cursor = conn.cursor()
        
        cursor.execute("""select * from usuario WHERE abv = ?""", (item_pesquisar,))
        teste = cursor.fetchall()

        return teste

        conn.close()

    def Le_tabela_usuario(self):
        conn = sqlite3.connect('bd_integracao.db')
        print ("banco conectado")
        cursor = conn.cursor()
        
        cursor.execute("""select * from usuario""")
        teste = cursor.fetchall()

        return teste
        conn.close()

    def setIdioma(self):
        banco = sqlite3.connect('bd_integracao.db')
        DB = banco.cursor()
        DB.execute("""SELECT idioma FROM config2""")
        idioma = DB.fetchall()
        return idioma

#Tabela Layouts
    def pesquisa_layout_banco(self, item_pesquisar):
        print("Pesquisa layout no Banco")

        conn = sqlite3.connect('bd_integracao.db')
        print("banco conectado")
        cursor = conn.cursor()

        cursor.execute("""select * from layout WHERE nome = ?""", (item_pesquisar,))
        teste = cursor.fetchall()

        return teste

        conn.close()


    def Le_tabela_layout(self):
        conn = sqlite3.connect('bd_integracao.db')
        print("banco conectado")
        cursor = conn.cursor()

        cursor.execute("""select * from layouts""")
        teste = cursor.fetchall()

        return teste
        conn.close()


    def cadastrar_layout_banco(self, layout):
        print("Pesquisa no Banco")

        conn = sqlite3.connect('bd_integracao.db')
        print("banco conectado")
        cursor = conn.cursor()
        print(layout)
        cursor.execute(""" INSERT INTO Layouts (nome) VALUES (?)""", (layout,))

        conn.commit()
        conn.close()

    def deletar_layout_banco (self, item):
            print("Deletar item do Banco")
            conn = sqlite3.connect('bd_integracao.db')
            print("banco conectado")
            print(item)
            cursor = conn.cursor()
            cursor.execute("""DELETE FROM Layouts WHERE nome = ? """,(item,))
            conn.commit()
            conn.close()